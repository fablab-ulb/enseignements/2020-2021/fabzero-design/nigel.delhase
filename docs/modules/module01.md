# GITLAB #

## 8 octobre ##

A mon arrivée au FabLab, c’est déjà bien rempli, tous attendent l’arrivée des professeurs et des assistantes. Ils étaient en retard.

Le local est prêt, je m’installe et à mon grand regret au pire endroit pour regarder la télé où sont projetés les cours, la table haute du professeur bloque une partie de la vue et je n’ai pas osé demander d’écarter un peu la table.

Je ne connaissais pas le programme de la matinée, mais je le découvrirai sitôt à l’arrivée du professeur Julien que je n’ai jamais vu.

Son cours commence, il s’agit d’une formation pour mieux utiliser Gitlab et HTML. Il fallait connecter mon ordinateur portable à internet mais il me fallait un ID.ULB et un mot de passe que je n’avais pas étant en séminaire jusqu’en décembre 2020. Je range mon ordinateur, inutile, je me contente d’être attentif et regarder la projection.

Le cours ou plutôt la torture mentale commence.

Je ne me souviens plus de comment ça a commencé mais je me souviens combien c’était horriblement incompréhensible, ce n’était pas la faute du professeur. Il avait sûrement bien expliqué et était bien patient.

J’ai toujours été nul en informatique et surtout HTML. A la fin du cours, j’ai presque regretté d’avoir choisi ce séminaire. C’était du charabia pour moi, trop compliqué à comprendre tout seul, il me fallait une aide pédagogique en privé pour m’apprendre à mieux utiliser GIT.

Mais ça a changé avec le cours de l’après-midi.

Le professeur avait parlé de l’application ATOM, mais je n’ai jamais compris cette application. Je suis désolé de le dire, je déteste ces logiciels, je n’en vois pas l’utilité pour ma vie professionnelle plus tard.

Le professeur de GIT a bien dit que c’est très compliqué au début, pour ma part, j’ai essayé de comprendre, en vain. C’est un cours que j’aimerais oublier.

## 7 novembre ##

Pour ma part, c’était le module où j’éprouvais le plus de difficultés, le codage n’a jamais été mon fort.

Pourtant, GIT est le plus important quand il s’agit de se documenter sur les travaux des modules 2,3 et 4. Je suis bien conscient de cela, alors, j’écris d’abord mes documentations des modules 2,3 et 4 pour ensuite tout taper avec le logiciel Word, c’est le plus facile pour moi.

Une fois terminé, j’envoie le tout au format PDF, par mail, au professeur Victor Levy.

J’obtiens un retour positif de mon travail sur Word, mais le professeur souhaiterait que j’importe tout sur GIT. J’étais assez désespéré, mais grâce aux autres étudiantes, j’ai vite reçu des propositions d’aide, notamment Clémentine et Margaux, j’ai bien sûr accepté.

Elles m’ont par la suite proposé d’importer tout ce que j’avais noté sur Word vers GIT pour moi et le faire à ma place. J’ai tout de suite refusé, je suis d’accord que l’on m’aide, m’explique comment faire, mais je souhaite par la suite me débrouiller seul.

Tout d’abord, Clémentine Benyakhou m’a expliqué via Messenger, tout ce que je dois faire avec GIT et comment travailler avec l’encodage, etc. Elle était à fond pour m’aider, c’était très sympa !

C’était aussi le cas de Margaux Derclaye, nous nous étions arrangés pour nous voir au FabLab afin qu’elle m’explique en pratique.

Clémentine avait constaté que j’avais un problème avec GIT sur mon ordinateur portable et m’a expliqué qu’un de ses amis avait eu le même cas et s’était adressé à Denis afin de régler mon problème. Je ne savais pas de quoi il s’agissait, je n’avais pas compris, mais ça avait l’air sérieux.

Une fois mon GIT réglé, Clémentine m’a réexpliqué une seconde fois, mais plus simplement en m’exportant sur Messenger des captures d’écran, des exemples et explications très imagées.

Il me restait qu’à essayer sur mon GIT, à commencer par mon Index.
Je commence par me présenter.

Il me faut d’abord me connecter sur GITLAB, sous mon nom nigel.delhasse et mot de passe.

![git](docs/images/CAPTURE_GITLAB_1.JPG)
 
Me voilà connecté, il me faut aller au groupe Fab-Zero-Design

![git](docs/images/CAPTURE_GITLAB_2.JPG)
 
Pour ensuite sélectionner le sous-groupe à mon nom même s’il est erroné par un S manquant à mon nom de famille. 
Ensuite cliquer sur docs puis sur index.nd pour y taper ma présentation personnelle.

![git](docs/images/CAPTURE_GITLAB_3.JPG)
![git](docs/images/CAPTURE_GITLAB_4.JPG)
 
Je clique sur l’icône « Editer » bleu pour en modifier le contenu, au départ c’était un texte d’exemple à supprimer.

![git](docs/images/CAPTURE_GITLAB_5.JPG)
 
Voici à quoi ressemble la page « Editer »

![git](docs/images/CAPTURE_GITLAB_6.JPG)
 
On y écrit tout ce qu’on veut, et quand on a terminé, on clique sur l’icône « Commit Changes » vert pour mettre à jour la modification.

D’après les explications de Clémentine, pour le titre, il faut taper dans « Editer » deux #, l’un au début du mot ou de la phrase et l’autre à la fin, tous deux suivis d’un espacement. 

Exemple ci-dessous

"# PRESENTATION PERSONNELLE #"

![git](docs/images/CAPTURE_GITLAB_7.JPG)
 
Pour un sous-titre, il faut appliquer le même procédé avec deux # de chaque côté du mot ou de la phrase : exemple ci-dessous.

"## ŒUVRE CHOISIE ##"

![git](docs/images/CAPTURE_GITLAB_8.JPG)

Si je veux mettre un mot ou une phrase en gras, il faut mettre, dans « Editer », deux ** de chaque côté sans espacement comme cet exemple ci-dessous.

"** matériaux recommandés **"

![git](docs/images/CAPTURE_GITLAB_9.JPG)
 
Si je veux mettre en italique, il faut faire pareil mais avec un seul * de chaque côté. Trois pour mettre en italique et en gras.

Pour importer des images dans « Editer » il faut commencer par écrire :

"![git] (docs/images/NOM_DE_L_IMAGE.jpg)"

Comme par exemple ci-dessous
 
![git](docs/images/CAPTURE_GITLAB_10.JPG)

Résultat après avoir cliqué sur « COMMIT CHANGES »

![git](docs/images/CAPTURE_GITLAB_11.JPG)

Mais avant cela, il me faut d’abord aller sur docs/images pour téléverser un fichier en cliquant sur l’icône ci-dessous.

![git](docs/images/CAPTURE_GITLAB_12.JPG)

Après avoir cliqué dessus, cliquer sur « cliquer pour envoyer » pour y télécharger une image, à condition qu’elle soit d’une taille inférieure ou égale à 10 mo.

![git](docs/images/CAPTURE_GITLAB_13.JPG)
![git](docs/images/CAPTURE_GITLAB_14.JPG)

Une fois l’image importée, il est indiqué qu’elle fait 0,2 mb, c’est bon. On clique sur l’icône « téléverser un fichier » vert pour télécharger et importer dans le document images de GIT.

Voilà l’import est effectué.

![git](docs/images/CAPTURE_GITLAB_15.JPG)

Il faut toujours bien nommer chaque image, en majuscule de préférence, pour faciliter le lien dans « EDITER ».

Il est vrai que c’est un peu lourd de devoir téléverser à chaque fois une image avant d’ « EDITER ». Il existe surement une méthode plus rapide pour téléverser un groupe d’image mais malheureusement, je ne sais pas encore comment faire.

Si la taille de l’image dépasse les 10 mb autorisés, il va me falloir l’ouvrir dans ADOBE PHOTOSHOP pour réduire la taille de l’image et l’enregistrer avant de le téléverser à nouveau dans docs/images.

D’après Clem, il existe un moyen de compresser un groupe d’images, tout en gardant leur qualité, en une action. Je ne maîtrise pas encore mais j’en apprendrai davantage plus tard, car il est vrai que devoir régler la taille de l’image sur PHOTOSHOP à chaque fois, ça me prend du temps.

Pour éditer un lien, il faut taper "[titre] (insérer lien)" comme cet exemple ci-dessous.

![git](docs/images/CAPTURE_GITLAB_16.JPG)

Après le "COMMIT CHANGES" le mot en bleu devient cliquable pour me diriger vers le lieu souhaité, vers YOUTUBE par exemple.

![git](docs/images/CAPTURE_GITLAB_17.JPG)

Jusqu’à maintenant, ce sont les seules actions que je maîtrise sur GIT, c’est pourtant simple et finalement assez amusant de travailler sur GIT. C’est assez pratique, je suis ravi d’avoir réussi à me débrouiller seul à éditer et à importer des images dessus. 

Je dois remercier celles qui m’ont aidé. 

J’avais déjà terminé d’éditer les modules 2,3 et 4.

Il me restait qu’à éditer ce module 1.


