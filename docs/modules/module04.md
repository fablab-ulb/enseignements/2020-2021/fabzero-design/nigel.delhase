# DECOUPAGE LASER #

## 22 octobre ##

Aujourd’hui, petite formation de découpage LASER, on apprend d’abord les bases telles que : quel matériau utiliser, déconseillé et interdit.

Ce cours est donné par Axel, un jeune formateur.

Le type de machine est LASERSAUR.

J’avais hâte d’apprendre plus sur le découpage LASER, je ne l’avais jamais fait. Ce serait très utile pour réaliser mes maquettes plus rapidement.

Le cours débute par une présentation de la machine et ses spécifications :

-	Surface de découpe : 122x61 cm
-	Hauteur maximale : 12 cm
-	Vitesse maximale : 6000 mm/min
-	Puissance du LASER : 100 W
-	Type de LASER : tube CO2 (infrarouge)

Ensuite les matériaux conseillés, déconseillés et interdits

**Matériaux recommandés :**

-	Bois contreplaqué (Polywood/Multiplex)
-	Acrylique (PMMA/plexiglas)
-	Papier, carton
-	Textile

**Matériaux déconseillés :**

-	MDF : fumée nocive et épaisse
-	ABS, PS : fond facilement et fumée nocive
-	PE, PET, PP : fond facilement
-	Composition à base de fibres : poussières très nocives
-	Métaux : impossible à découper

**Matériaux interdits :**

-	PVC : Fumée acide et très nocive
-	Cuivre : réfléchit totalement le LASER
-	Teflon (PTFE) : Fumée acide et très nocive.
-	Vinyl, simili cuir : peut contenir de la chlorine
-	Résine phénolique, époxy : fumée très nocive

On nous a appris à connaitre les précautions à prendre avant d’utiliser la machine de découpe laser :

-	Connaitre avec certitude quel matériau est découpé
-	Toujours ouvrir la vanne d’air comprimé !
-	Toujours allumer l’extracteur de fumée !
-	Savoir où se trouve le bouton d’arrêt d’urgence
-	Savoir où se trouve un extincteur au CO2

On nous a informé quel format utiliser, seulement des fichiers vectoriels :

-	SVG : efficace
-	DXG : plus difficile d’exporter un fichier compatible

Voilà les bases acquises, on nous montre la vraie machine et comment la manœuvrer.

D’abord, effectuer la liste de précautions ci-dessus.

Ouvrir le couvercle de la machine, puis accéder à l’interface, cliquer sur le bouton RUN HOMING CYCLE pour faire revenir la tête à l’origine.

Ouvrir un fichier SVG et mettre le matériau dans le couvercle. Ne pas fermer le couvercle.

Déplacer la tête de découpe au bon endroit et la positionner au-dessus du matériau avec le bouton MOVE ou JOG.

Si nécessaire, régler la distance focale avec le support de 15 mm et déplacer le dessin avec le bouton OFF SET.

Vérifier que le dessin ne dépasse pas du matériau avec le bouton RUN BOUNDING BOX.

Sélectionner une couleur ou une image avec le bouton +, il s’agit de sélectionner les tracés pour différer la découpe s’il y a plusieurs couleurs, choix entre découpe et gravure en fonction de la puissance du LASER.

Régler la vitesse et puissance de découpe.

Fermer le couvercle et le bouton STATUT se virera au vert, ça signifie que la machine est prête à faire la découpe, si ce n’est pas le cas, c’est qu’il y a un problème.

Appuyer sur le bouton RUN pour démarrer la découpe.

Appuyer sur PAUSE pour pause et sur STOP pour arrêter.

Voila comment se conclut le cours sur découpage LASER, j’ai encore besoin de pratique pour mieux comprendre la machine et je ne sais pas encore comment exporter des fichiers AutoCad vers SVG. J’ai encore beaucoup à apprendre.

## 29 octobre ##

Pour maitriser le programme et la machine de découpe LASER, on nous a demandé de créer un objet au design inspiré de l’œuvre choisie au musée ADAM.

On nous a imposé d’utiliser le polypropylène comme matériau. Nous n’avons le droit que d’utiliser un format A3 de ce matériau pour concevoir une lampe de la forme que nous souhaitons, et sans utiliser vis, colle etc. nous devons pouvoir l’assembler et le désassembler.

Et comme le polypropylène est souple, je pourrais utiliser la résistance et les courbes pour faire tenir l’assemblage sans qu’il ne se désassemble facilement.

Avant cela, je dois faire des essais pour mieux maitriser la machine et l’exportation des fichiers vers SVG, je n’ai jamais fait tout ça !
J’opte pour la création d’une sphère à partir d’un assemblage classique.

J’utilise d’abord Autocad, programme que je maitrise mieux que INKSCAPE, qu’Axel m’a conseillé, je ne veux pas perdre de temps.

Je dessine d’abord un plan de travail au format A3, 297mm x 420mm pour mieux visualiser l’ensemble des pièces que je vais découper.
Mes dessins effectués.

![git](docs/images/CAPTURE_AUTOCAD.JPG)
 
Je l’enregistre au format DXG et j’ai demandé à Axel si je pouvais déjà utiliser DXG de l’AutoCad directement au découpage LASER, il a dit que c’était compliqué et m’a conseillé de l’importer vers Illustrator pour l’enregistrer au format SVG.

Je n’avais pas bien compris comment m’y prendre, alors là, j’ai demandé de l’aide à un élève, malheureusement, j’ai honteusement oublié de lui demander son prénom, ainsi que à un autre étudiant du nom de Dimitri.

Ils m’ont expliqué comment faire, je les remercie chaleureusement !

D’abord, j’enregistre mon dessin sous DXG, J’ouvre ensuite Illustrator pour un nouveau document A3 et j’importe le fichier DXG à sa taille originale.
 
![git](docs/images/CAPTURE_ILLUSTRATOR_1.JPG)
![git](docs/images/CAPTURE_ILLUSTRATOR_2.JPG)
![git](docs/images/CAPTURE_ILLUSTRATOR_3.JPG)

L’échelle me semble parfaite, je l’enregistre au format SVG. Enfin mon dessin sous SVG est créé, je le mets sur clé USB, paré pour mon premier découpage LASER.

Comme il s’agit de mon premier découpage au LASER, j’ai préféré demander de l’aide à un autre étudiant expérimenté, il s’agit du même étudiant dont je ne connais toujours pas le nom, qui m’avait aidé avec Illustrator.

Il m’a accompagné jusqu’à la salle des machines.

La première chose que j’ai en tête est d’effectuer la liste des précautions, la machine est déjà mise en marche, j’ouvre la vanne d’air comprimé, l’extracteur de fumée est déjà en marche aussi.

Malheureusement, je n’entends pas, mais l’étudiant m’en a assuré le fonctionnement.

J’ouvre le couvercle et j’y dépose une plaque de polypropylène.

L’autre étudiant ouvre l’interface et insère la clé USB afin d’en ouvrir le fichier SVG.

Nous déplaçons le viseur à l’origine et je déplace le coin de la plaque juste en dessous et nous vérifions que le dessin ne dépasse pas le matériau à l’aide de MOVE.

Tout est bien en ordre et nous paramétrons la vitesse à 1500 et la puissance à 60%, la hauteur du LASER est bien réglée.

Je ne fais pas de gravure, donc, une seule couleur pour le découpage total.

Je ferme le couvercle et clique sur RUN pour démarrer, ça n’a pas fonctionné, car nous avons oublié de préciser la couleur à découper.
Une fois le problème réglé, ça démarre.

On m’a expliqué que ça durait environ 15-20 minutes pour être sûr, le temps de laisser la machine extraire totalement la fumée dans le couvercle.
20 minutes plus tard, le découpage est terminé, je suis surpris de la propreté du résultat.

Je récupère les pièces et j’essaie mon premier assemblage LASER, c’est l’échec, car les assemblages ne sont pas assez serrés, ça ne tient pas bien, il faut tenir toutes les pièces ensemble en même temps pour que ça ne s’écroule pas, c’était mission impossible sans colle.

J’ai quand même essayé de la monter avec de la colle, juste pour voir si l’assemblage marche bien.

![git](docs/images/ASSEMBLAGE_1.jpg)
 
Je pense le refaire, en réduisant l’espace entre les assemblages afin de le rendre plus serré à monter, ça tiendra mieux.
Ce n’était qu’un essai pour mieux comprendre comment créer un fichier SVG et comment utiliser la machine. Le vrai travail commence ici.

**2e ESSAI**

D’abord je fais des croquis, je m’inspire de la silhouette de mon objet choisi, pour en faire un support principal 

![git](docs/images/CONCEPT_1.jpg)

J’ai des idées et calculs en tête, je commence à dessiner sur Autocad avant de l’enregistrer sous DXF R12 pour l’importer à l’échelle originale sur Adobe Illustrator.

![git](docs/images/CAPTURE_AUTOCAD_2.JPG)
![git](docs/images/CAPTURE_ILLUSTRATOR_4.JPG)
 
Je l’enregistre sous SVG et direction la salle des machines à découpe LASER.

Je fais des vérifications de la liste de précautions et j’importe le fichier SVG sur l’interface. Je fais comme la dernière fois.
 
Je mets juste une couleur ROUGE pour découpage total. Vitesse 1500 et puissance 100%, je voulais mettre 50% mais j’avais oublié. Je ferme le couvercle et je mets sur RUN.

**Résultat du découpage**

![git](docs/images/ASSEMBLAGE_2.jpg)
 
Cette fois, je réussis l’assemblage sans tricher.

**RESULTAT DE L’ASSEMBLAGE DE LA LAMPE-BOUGIE**
 
![git](docs/images/ASSEMBLAGE_5.jpg)
 
**ESSAI DANS LA PENOMBRE AVEC BOUGIE**

![git](docs/images/LAMPE_1.jpg)
![git](docs/images/LAMPE_2.jpg)
             
J’ai pensé ajouter des motifs, j’avais discuté avec les autres étudiants s’il valait mieux mettre des motifs géométriques ou pas. J’ai fait des essais. 

