# IMPRESSION 3D #

## 22 octobre ##

C’est le jour de ma première impression 3D, mais j’ignorais comment utiliser correctement l’application Prusa Slicer.
Hélène s’est chargée de me rappeler. J’ouvre l’application Prusa.

![git](docs/images/CAPTURE_IMPRESSION_3D_1.JPG)
 
Je sélectionne le type de filament et le type de machine.
J’importe le Fichier STL de mon premier ESSAI et l’objet y apparaît automatiquement.
 
![git](docs/images/CAPTURE_IMPRESSION_3D_3.JPG)

L’objet est déjà placé dans une position très stable, pas besoin de pivoter.
Je réglé les couches et périmètres avec Hélène. Elle m’a conseillé 0.2 mm, suffisant.

![git](docs/images/CAPTURE_IMPRESSION_3D_4.JPG)

Je règle le remplissage, comme mon objet ne comporte pas d’élement solide et rempli, Hélène me dit que la densité du remplissage de 15% suffira et le motif de remplissage rectiligne est idéal.

![git](docs/images/CAPTURE_IMPRESSION_3D_5.JPG)
 
On règle les réglages de la jupe et des bordures. L’objet est très stable alors pas besoin de grande jupe, j’ai mis la largeur de la bordure à 3mm et 2 couches seulement.
 
![git](docs/images/CAPTURE_IMPRESSION_3D_6.JPG)

Je clique sur Générer les supports dans les réglages des supports, c’est déjà réglé automatiquement.

![git](docs/images/CAPTURE_IMPRESSION_3D_7.JPG)
 
Retour au plateau, je clique sur Découper maintenant pour avoir un aperçu de simulation de l’impression 3D, sa durée, la quantité de filament utilisé et son coût.

![git](docs/images/CAPTURE_IMPRESSION_3D_8.JPG)

Tout est en ordre, je clique sur EXPORTER le G-Code.

Pour importer des fichiers G-Code dans la machine Prusa, il faut une carte SDD mais je n’ai pas de lecteur SDD sur mon ordinateur portable alors je l’enregistre dans la clé USB puis je le donne à Hélène pour qu’elle importe le fichier dans la carte SDD avec son ordinateur portable.

Nous allons ensemble à la salle des machines d’impression 3D, Hélène m’apprend à manipuler la machine, c’est si simple. J’insère la carte SDD, l’import se fait automatiquement, je fais tourner le bouton pour sélectionner une option ou un fichier et je pousse le bouton pour confirmer.

La machine doit d’abord chauffer le filament et le plateau. (je ne dois pas oublier de sélectionner le type de filament et j’appuie sur l’option PLA)
Quand le filament est chauffé à 215 degrés, la machine ejecte le PLA et me demande si la couleur correspond, je pousse le bouton sur YES si c’est le cas, sinon je le pousse sur NO et la machine va encore éjecter jusqu’à avoir la couleur correspondante.

La machine commence à imprimer mon premier modèle 3D, je dois rester surveiller les trois premières couches, histoire de voir s’il n’y pas de dérèglement. Ce ne fut pas le cas, je n’ai qu’à attendre 2h50 avant la fin de l’impression

**RESULTAT DE LA PREMIERE IMPRESSION 3D**

![git](docs/images/PHOTO_CHAISE_3.jpg)

J’étais content du premier résultat d’impression 3D, il était solide à ma surprise, mais il y avait un petit défaut d’impression, deux petits trous à l’avant de l’assise. Voir ci-dessous. 

![git](docs/images/PHOTO_ZOOM_CHAISE.jpg)

La machine n’était pas consciente de l’erreur d’impression, c’est trop fin à mon avis, j’ai pensé refaire un deuxième essai, j’épaissirais le dossier et l’assise pour ne plus rencontrer le même problème.

## 2E ESSAI ##

Importation du fichier STL du 2e ESSAI sur le plateau, il y apparait debout, pas une position stable.

![git](docs/images/CAPTURE_IMPRESSION_3D_9.JPG)
 
Je le pivote pour le mettre du coté couché comme le premier ESSAI, pour une meilleure stabilité.

![git](docs/images/CAPTURE_IMPRESSION_3D_10.JPG)
 
J’utilise les mêmes réglages d’impression que pour le précédent objet, je n’ai qu’à appuyer sur Découper maintenant. Et voici la simulation d’impression 3D.

![git](docs/images/CAPTURE_IMPRESSION_3D_11.JPG)
 
Il me reste qu’à enregistrer le G-Code dans la clé USB et le donner à une des assistantes pour l’importer dans la carte SDD.
Cette fois, j’ai appris comment changer la couleur du filament, j’ouvre l’imprimante et je sélectionne l’option UNLOAD the filament, mais avant cela, je dois attendre que ça chauffe jusqu’à 215 degrés avant d’appuyer sur le bouton pour enlever le filament.


Je change de rouleau de filament en PLA, j’insère le filament dans le trou de l’éjecteur, ensuite j’appuie sur LOAD the filament, la machine gère seule jusqu’à éjecter une partie et me demander de confirmer la couleur comme la dernière fois.

J’inséré la carte SDD et je sélectionne mon 2e ESSAI, je confirme et l’impression commence.

Cette fois, ça dure 3h30 à cause de sa forme plus complexe que le premier.

**RESULTAT DE LA DEUXIEME IMPRESSION 3D**

![git](docs/images/PHOTO_CHAISE_4.jpg)
![git](docs/images/PHOTO_CHAISE_5.jpg)
             
Le design est plutôt intéressant et amusant à regarder. C’est bien détaillé et ce fut une belle évolution comparée à la première impression.
L’impression a reçu des petits défauts, mais je m’y attendais à cause des quatre trous, les bords étaient trop fins, presque coupants, donc plus fragiles.

J’avais espéré que la prochaine impression serait un succès.

## 3e ESSAI ##

Importation du fichier SVG de l’ESSAI 3

![git](docs/images/CAPTURE_IMPRESSION_3D_12.JPG)
 
Même position couchée et mêmes réglages d’impression que les deux précédentes.
Résultat de la simulation.

![git](docs/images/CAPTURE_IMPRESSION_3D_13.JPG)

Enregistrement du fichier G-Code dans la carte SDD
Il me reste qu’à l’imprimer, j’ai choisi le filament gris-noir. Plus sobre. Je connais les étapes.
Il y avait de légers défauts d’impression à cause du filament entremêlé, les couches se sont très légèrement écartées, rien de grave mais un peu décevant quand même.

L’impression a duré 2h30.

**RESULTAT DE LA TROISIEME IMPRESSION 3D**

![git](docs/images/PHOTO_CHAISE_6.jpg)
![git](docs/images/PHOTO_CHAISE_7.jpg)

Malgré quelques petits défauts, cette impression est une réussite, j’ai obtenu une belle réplique de la chaise à bascule, l’ENDLESS de Dirk Vander Kooij.

## EVOLUTION DES OBJETS EN 3D IMPRIMES ##

![git](docs/images/PHOTO_EVOLUTION.jpg)
