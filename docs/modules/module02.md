# FUSION 360 #

## 15 Octobre ##

La journée commence par un petit cours donné par Gwen et Hélène, une petite formation d’utilisation de la machine d’impression 3D. J’avais hâte d’en apprendre plus sur ces machines, j’ai toujours voulu essayer d’imprimer un objet 3D.

Encore un problème de connexion internet avec mon ordinateur portable mais vite résolu par Hélène qui m’a gracieusement donné son accès internet.

Ce cours explique les paramètres de la machine, ses composants et indique aussi quelle application utiliser pour importer des fichiers STL mais j’avoue avoir oublié d’installer cette application. Je peux remercier la puissance et vitesse du réseau internet pour le télécharger et l’installer directement, heureusement le logiciel est léger.

C’est déjà la fin de la formation, ça a duré à peine une heure, c’est là qu’intervient Victor Levy, il nous demande de recréer l’objet que nous avions choisi au musée ADAM. C’est avec tout ce qu’on avait appris la semaine dernière sur le programme FUSION 360, que l’on va recréer cet objet.
A ce moment, je suis déjà en train de réfléchir au procédé de création de la chaise à bascule de Dirk Vander Kooij, je me suis dit que ça va être du Gâteau avec tous ces courbes et trous courbés !

On déménage dans un autre local, je m’installe et ma première conception commence.
Tout d’abord, j’essaie de comprendre l’objet en faisant des dessins, en analysant ses lignes et ses formes géométriques.

![git](docs/images/CONCEPT_2.jpg)
 
J’ouvre FUSION 360, crée une nouvelle conception, je commence par « créer une esquisse », c’est un peu comme AutoCad mais je n’ai jamais autant travaillé avec des courbes. C’était assez dur mais finalement j’y suis arrivé.

Ma première esquisse (une face) de la chaise était très rigide, pas de courbe mais je me suis dit que c’était la première fois que je faisais une esquisse sur FUSION 360, je pouvais quand même être fier de moi, même si je n’étais pas satisfait, c’était un essai.

L’esquisse terminée, je fais une extrusion de la face, mon premier modèle 3D de FUSION 360 est créé, c’était assez amusant.

Le problème est que les trous doivent être courbés et je ne savais pas comment m’y prendre, j’ai essayé le perçage.

Ça ne marche pas, certes, le trou est « courbé », mais le bas est à l’air, on voit au travers, il faut le fermer. C’est cela qui me rend perplexe, je me demande comment je peux faire ?

Je demande au professeur Victor Levy, il m’a dit de me débrouiller seul.

J’essaie de trouver une solution, je regarde les dessins et la structure de mon siège à bascule. J’ai une idée !

Je dessine deux faces, celle du coté et celle du milieu, ensuite, je fais un lissage entre les deux faces pour créer un nouvel objet. Je ferai une symétrie verticale de l’objet pour terminer.

Je ne savais pas si ça allait marcher, mais ça valait le coup d’essayer.

## ESSAI 1 ##

Sur FUSION 360, je dessine les deux faces via l’option « créer une esquisse » tout en suivant et respectant les proportions de la chaise originale. 

Esquisse de la face du côté

![git](docs/images/CAPTURE_1E_ESSAI_1.JPG)
 
Esquisse de la face du milieu

![git](docs/images/CAPTURE_1E_ESSAI_2.JPG)
 
Je mets la face du milieu au centre de XYZ afin de faciliter la symétrie miroir ensuite.
Celle du coté est déplacée 20 mm vers la droite.
J’effectue le lissage DIRECTION-DIRECTION des deux faces ci-dessous

Création de l’objet 1

![git](docs/images/CAPTURE_1E_ESSAI_3.JPG)
 
Il y a un problème, il n’y a pas de trou au pied de la chaise, c’est problématique mais j’ai sitôt trouvé une solution.
D’abord faire le lissage de chaque surface des trous individuellement pour créer de nouveaux objets 2,3,4.

Même lissage DIRECTION-DIRECTION

![git](docs/images/CAPTURE_1E_ESSAI_4.JPG)
 
C’est le moment de soustraire les objets 2,3,4 de l’objet 1 pour en faire des trous courbés, c’était ma théorie, il fallait l’essayer. J’essaie de trouver l’option soustraction, je ne trouve pas, ce n’est pas comme SKETCHUP, je ne vois que l’option « scinder un corps » dans MODIFIER et je l’essaie.

Je sélectionne l’objet 1 comme corps à scinder et les objets 2,3,4 comme outils de scission.

![git](docs/images/CAPTURE_1E_ESSAI_5.JPG)
 
Je désactive ensuite l’œil des corps 2-7 sauf le corps 1 et voilà le résultat

![git](docs/images/CAPTURE_1E_ESSAI_6.JPG)
 
Voila la dernière étape, la symétrie miroir dans CREER.
Je sélectionne le type du corps, sélectionne l’objet et le plan de symétrie. Opération JOINDRE
Résultat final !

![git](docs/images/CAPTURE_1E_ESSAI_7.JPG)
 
Il me reste que à l’enregistrer pour éviter la perte de travail.
Je suis super satisfait d’avoir réussi mon premier modèle 3d et d’avoir réussi à trouver le procédé à moi seul.
C’était très amusant !

Au début, j’avais oublié vers quel format exporter mon objet, Hélène m’a rappelé que ça doit être exporté sous STL.
L’exportation effectuée, mon objet est en route pour ma première impression 3D.
Néanmoins je souhaiterais faire une amélioration du modèle 3D avec plus de courbes.

# ESSAI 2 #

Je refais le modèle plus courbé en suivant les mêmes étapes, à commencer par esquisser les deux faces, celle du milieu et celle du coté droit, en faisant plus de courbes.

Les voici ci-dessous.

![git](docs/images/CAPTURE_2E_ESSAI_1.JPG)
 
J’effectue un lissage DIRECTION-DIRECTION des deux faces et voila le résultat très dérangeant ci-dessous.

![git](docs/images/CAPTURE_2E_ESSAI_2.JPG)

 
Le lissage ne marche pas, je ne comprends pas et je commence à paniquer. J’essaie toutes les options du lissage, quand je clique sur CONSERVER les arêtes tangentes, ça marche mieux malgré un trou au dossier de la chaise. Voir ci-dessous.

![git](docs/images/CAPTURE_2E_ESSAI_3.JPG)

Ce n’est pas grave, je continue, je fais le même lissage avec les faces d’un des 3 trous et ca ne marche pas avec l’option CONSERVER

![git](docs/images/CAPTURE_2E_ESSAI_4.JPG)
 
Mais ça marche quand je mets sur FUSIONNER les arêtes tangentes.

![git](docs/images/CAPTURE_2E_ESSAI_5.JPG)
 
C’est étrange, je ne comprends pas ce phénomène mais je continue et fais de même avec les autres trous restants. Ensuite je pratique le même procédé de sciage que lors du premier essai et voici le résultat :

![git](docs/images/CAPTURE_2E_ESSAI_6.JPG)

Reste la symétrie pour arriver au résultat final :

![git](docs/images/CAPTURE_2E_ESSAI_7.JPG)
 
Je l’exporte sous STL.

Le deuxième essai est nettement mieux que le premier avec ses courbes, ça rend le design plus attirant malgré ses quatre trous. Ça ne correspond pas totalement à l’original mais j’adore son style sportif et futuriste.

# 3e ESSAI #

Le deuxième essai était bien, ça ne correspond toujours pas mais il y a du progrès, maintenant je pense que dessiner séparément le haut et le bas de la chaise pour ensuite les fusionner pourrait être la meilleure solution.

Sur FUSION 360, e commence par le haut mais avant ça, je fais une esquisse de l’ensemble pour comparer et voir les repères.

![git](docs/images/CAPTURE_3E_ESSAI_0.JPG)
 
Les esquisses des faces du haut de la chaise sont terminées.
 
![git](docs/images/CAPTURE_3E_ESSAI_1.JPG)

Je pratique le même procédé de lissage que les précédents essais, mais cette fois, c’est CONNECTE-DIRECTION et CONSERVER.

![git](docs/images/CAPTURE_3E_ESSAI_2.JPG)
 
La forme et la courbe sont nettement mieux réussies que les précédentes.
Maintenant il faut esquisser les deux faces du bas de la chaise

![git](docs/images/CAPTURE_3E_ESSAI_3.JPG)
 
Le lissage des deux faces n’a pas fonctionné comme je le souhaite, j’ai essayé toutes les options, en vain.

![git](docs/images/CAPTURE_3E_ESSAI_4.JPG)

Ca commence à m’agacer, je mets cela de coté et je commence à lisser les faces des trois trous.
Lissage DIRECTION-DIRECTION, option FUSIONNER

![git](docs/images/CAPTURE_3E_ESSAI_5.JPG)
 
Les trois objets effectués, je retourne aux esquisses des deux faces du bas du siège pour supprimer toutes les lignes, sauf leur contour pour en faire un lissage plus facilement.

Lissage des deux faces, DIRECTION-DIRECTION avec option CONSERVER. 

Résultat

![git](docs/images/CAPTURE_3E_ESSAI_6.JPG)

Ensuite, j’effectue l’option Scinder les corps pour supprimer les trous.
Même procédé que les précédents essais.

Résultat

![git](docs/images/CAPTURE_3E_ESSAI_7.JPG)
 
Il est temps de fusionner les deux parties pour n’en faire qu’une avec l’option COMBINER dans MODIFIER

![git](docs/images/CAPTURE_3E_ESSAI_7.5.JPG)

Voila le résultat de la fusion, il me reste à faire la symétrie verticale et le modèle sera terminé.

![git](docs/images/CAPTURE_3E_ESSAI_8.JPG)
![git](docs/images/CAPTURE_3E_ESSAI_9.JPG)
 
**RESULTAT FINAL**

![git](docs/images/CAPTURE_3E_ESSAI_10.JPG)
 
Exportation sous STL et paré pour impression 3D.

Cette fois, je suis totalement satisfait du résultat du troisième essai, j’en suis fier !
Ce fut une belle évolution, certes, j’ai encore beaucoup à apprendre du logiciel FUSION 360, ce n’est pas encore parfaitement maitrisé.
Il y a encore énormément de choses que je ne connais pas ou dont j’ignorais l’existence.

