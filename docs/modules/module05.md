# FORMATION CNC SHAPER ORIGIN #

Le 1er décembre

J’avais réservé une place pour une petite formation CNC avec une machine appelée Shaper Origin avec Axel, les places sont limitées par deux groupes de 7 personnes maximum pour des raison de sécurité liée au COVID.

Il s’agit d’un appareil manuel qui perce et fraise avec une précision numérique.
Victor Levy nous a conseillé de regarder des tutoriels en vidéo sur le site Shaper Origin.
Il y a trois tutoriels en vidéo très clairs qui nous expliquent comment débuter, quelles mesures prendre, et être attentif.

-	Première leçon : Déballage.
-	Seconde leçon : Aircut.
-	Troisième leçon : premier fraisage.

## PREMIER TUTORIEL ##

**Lien** [vidéo](https://www.youtube.com/watch?v=Uu5kVJtEwXM&feature=emb_logo&ab_channel=Shaper)

Je vais commencer par regarder la première leçon.

On nous décrit d’abord la boîte de rangement de la machine, c’est un systrainer de 10 OZ, efficace pour le transport sécuritaire. La boîte contient un adapteur pour l’aspirateur qui permet de connecter l’aspirateur avec Origin au cas ou l’aspirateur ne serait pas compatible avec Origin.

Toujours utiliser l’aspirateur à chaque utilisation de Origin.

La machine Shaper Origin comporte d’une poignée de transport pour la sortir en toute sécurité hors de la boîte, avec beaucoup de précautions. Elle coûte 3000 euros environ !

La machine est équipée d’un écran de protection magnétique, qui nous protège des petits débris quand on fraise. Et toujours enlever la chose bleue qui sécurise le mécanisme lorsqu’on souhaite utiliser la machine, sinon ça bloquerait le mouvement du mécanisme lors des fraisages. Ça provoquerait de gros problèmes de destruction du mécanisme s’on oublie de l’enlever.

Toujours remettre la chose bleue lorsqu’on range la machine dans la boîte, très important pour éviter tous dégâts lors des transports.
La boîte contient aussi des accessoires comme, des mèches, des Shapertapes de 45 m, trois fraises de base, une clé hexagonale de 4 mm et une clé anglaise de 19 mm.

Et bien sûr un manuel d’utilisation et un résumé de références de couleurs ainsi qu’un code de référence.

Maintenant, on va commencer par présenter la machine.

La machine comporte deux poignées très ergonomiques aux mains de l’utilisateur afin de déplacer la machine, sur chacune de ces poignées, il y a un bouton, le rouge à gauche et le vert à droite. Le vert est pour le démarrage de la fraise et le rouge, le bouton d’arrêt.
Ils ne fonctionnent pas si la machine n’est pas branchée.

La machine est équipée d’un écran au-dessus de la fraiseuse et d’une caméra sous la poignée de transport, cette caméra sert à repérer l’environnement pour mieux comprendre le plan de travail, elle analyse l’environnement grâce aux Shapertapes collés sur le plan de travail devant la caméra.
Les Shapertapes sont essentiels pour pouvoir utiliser la machine, d’après Axel, un rouleau de Shapertape coûte 16 euros, c’est cher s’on gaspille. On reparlera en profondeur des Shapertapes après le premier tutoriel.

Pour régler ou changer la fraise, toujours retirer la prise électrique ainsi que le câble relié à la fraiseuse, pour une question de sécurité.
L’interrupteur de marche/arrêt de la fraiseuse est visible devant l’utilisateur, cet interrupteur est seulement relié à la fraiseuse, il est indépendant de l’ordinateur et du moteur d’Origin.

On peut régler la vitesse de la fraiseuse sur le côté droit et dévisser avec la clé hexagonale de 4mm pour retirer la fraiseuse vers le haut. On utilise la clé anglaise pour retirer la fraise de la fraiseuse pour la changer.

La fraiseuse bouge de haut en bas dans le diamètre de correction dans l’axe X et Y, ce système de mouvement permet à la fraiseuse de fraiser en mouvement automatique avec une grande précision.

En bas de la fraiseuse, il y a un trou qui permet à l’aspirateur d’aspirer les débris de matériaux lors du fraisage, cela permet de travailler proprement continuellement. L’installation de l’aspirateur est très simple.

Pour la réinstallation de la fraiseuse, il y a un indicateur pour ou insérer correctement, on visse avec la clé hexagonale sans être agressif, c’est léger à visser.

La présentation de la machine semble être terminée, je vous ai tout expliqué comme le tutoriel.

Passons au tutoriel suivant.

## DEUXIEME TUTORIEL ##

**Lien** [vidéo](https://www.youtube.com/watch?v=sdsMOPZFMRQ&feature=emb_logo&ab_channel=Shaper)

Le second tutoriel explique le fonctionnement de l’Aircut, il s’agit de l’étape d’expérimentation du fichier SVG avec l’ordinateur qui analyse l’environnement à l’aide des Shapertapes.

On branche la prise de l’ordinateur et on l’allume, penser à débrancher la fraiseuse et retirer la chose bleue, c’est très important !

L’interface de l’écran se connecte au Wifi et au compte ShaperHub pour accéder aux fichiers que l’on veut télécharger ou partager sur le cloud. Une fois cette étape d’identification franchie, il est temps d’installer des Shapertapes sur le plan de travail.

C’est la première chose à faire avant de démarrer le travail de fraisage.

On dépose plus de trois lignes de Shapertape, espacés entre eux d’environ 10 cm, ce n’est pas grave si une ligne n’est pas parfaitement alignée aux autres, il est plus important que la caméra reconnaisse les points des dominos pour mieux analyser l’environnement en 3D Pour commencer à scanner on appuie sur le bouton vert sur l’interface de l’écran, l’ordinateur commence à scanner les tapes. Sur l’écran, les tapes virent au bleu, ce qui signifie que ça a bien scanné.

On scanne en bougeant la machine, j’avais oublié de préciser. Surtout, toujours garder la machine sur le plat de la surface !
Pour terminer, appuyer sur FIN sur l’interface.

Là, la machine va enregistrer les points de repères, une fois enregistré, il y a un point qui apparaît sur l’écran, c’est un point qui indique le centre de la fraise.

Double Tap sur l’écran pour dégommer. Les dominos noirs signifient qu’ils ne sont pas reconnus par la machine, mais peu importe, tant qu’elle a reconnu le reste (ceux en bleu).

D’après Axel, les bandes doivent comporter au moins 4 dominos pour que l’ordinateur analyse mieux l’environnement. Dans la vidéo, l’homme a mis trop de dominos, ce qui est un sacré gaspillage. Axel nous a conseillé d’au moins essayer de récupérer les bandes si possible.

Cette étape passée, on peut insérer la clé USB pour importer un fichier SVG pour débuter le fraisage.

Voyons les étapes. Cliquer sur le bouton + pour sélectionner et placer le fichier SVG sur le plan de travail.
L’aperçu apparait sur l’écran, déplacer, ajuster sur le plan de travail et cliquer sur le bouton vert PLACE pour confirmer, ou sur le bouton rouge CANCEL pour annuler.

Possibilité de tourner le fichier, rotation de 45° par clic.

Voila le fichier est placé, les traits animés apparaissent sur l’écran, c’est le retour au zoom avec le point indiquant le centre de la fraise. Là, c’est toujours en mode Design. Si on veut commencer à fraiser, cliquer sur CUT pour changer de mode.

En mode Fraiser, il y a des paramètres à gauche de l’écran, toujours couper le courant de la fraiseuse et débrancher quand on règle les paramètres pour plus de sécurité.

L’icone d’une forme de domino au-dessus à droite de l’écran, indique la qualité du Shapertape, ça vire à l’orange s’il y a un problème, la qualité s’appauvrit et l’ordinateur perd les repères, ce qui gâcherait le découpage. Heureusement, si ça vire au rouge, la fraise va se retirer automatiquement pour sauver le travail.

Sur le plan de travail, lorsqu’in déplace le centre de la fraise au-dessus ou à proximité des traits du fichier SVG, ça vire au noir, la ligne s’épaissit et vire au noir.

Ça indique où la fraise va découper, c’est comme un guide, un aperçu réel de découpage. Les lignes noires apparaissent à chaque fois où le cercle du centre de la fraise passe ou touche les traits.

On peut régler l’épaisseur via les paramètres à gauche de l’écran, ou même commander afin que la machine coupe sur la ligne ou bien à l’extérieur de la ligne ou à l’intérieur de la ligne. On peut même demander à la fraiseuse de fraiser tout ce qui se trouve à l’intérieur du contour pour faire une poche ou à l’extérieur, mais c’est plus compliqué, ça dépend du dessin.

Il y a le paramètre de hauteur de fraisage, mais attention de ne pas fraiser trop profondément, car ça risque d’abîmer ou de surchauffer la fraise. Il faut fraiser petit à petit, c’est conseillé.

Appuyer sur le bouton vert pour commencer le fraisage, là le centre de la fraise est automatiquement dirigé vers la ligne noire, c’est le mode Aircut, ça se voit quand le point bleu apparait sur l’écran. On déplace la machine à l’aide des poignées et la fraise suivra automatiquement la ligne noire, quand cette dernière vire au bleu, ça signifie que cela a été fraisé.

Pour que la fraise suive bien la ligne, il ne faut pas que le cercle autour du centre de la fraise quitte les traits, ils doivent rester en contact.

Il y a un mode auto, c’est là où la fraise va continuer à fraiser jusqu’au bord du cercle, même si je ne bouge pas la machine, c’est plus propre, la fraiseuse suivra mieux son rythme comme ça. Une fois le fraisage terminé, appuyer sur le bouton rouge pour remonter la fraiseuse.

Voila, le deuxième tutoriel terminé.

Passons au troisième tutoriel qui va montrer la machine en action.

## TROISIEME TUTORIEL ##

**Lien** [vidéo](https://www.youtube.com/watch?v=DekAjAOIVvQ&feature=emb_logo&ab_channel=Shaper)

Pour faire un fraisage, il nous faudrait des lunettes de protection un casque audio, mais inutile pour moi, vu ma surdité.

On peut utiliser des doubles Tapes pour stabiliser le plan de travail. Ce serait mieux d’avoir un double panneau, un pour protéger le plan de travail, l’autre à fraiser.

Pour coller ces deux panneaux entre eux, utiliser la double tape pour améliorer la stabilité. Si on veut renforcer davantage, on peut utiliser des serre-joints. C’est plus efficace que les doubles tapes seules.

Une fois le plan installé et sécurisé, placer les Shapertapes comme l’avait expliqué le deuxième tutoriel, puis on passe au scan. L’homme a mis trop de Shapertapes, il se moque totalement du gaspillage et répète ce qu’il avait expliqué au deuxième tutoriel.

Le scan terminé, retirer l’appareil de fraisage pour y insérer une fraise à l’aide d’une clé hexagonale.

Ne pas insérer trop profondément la fraise. Bien serrer la fraise avec une clé anglaise. Remettre la fraiseuse à sa place avant de resserrer avec la clé hexagonale.

Ne pas oublier l’écran de protection.

On peut brancher le courant de la fraiseuse sans la mettre en marche. D’abord sélectionner un fichier SVG via une clé USB en cliquant sur + comme déjà expliqué au précèdent tutoriel.

Une fois le SVG placé, se mettre en mode Fraisage en cliquant sur CUT, paramétrer la profondeur, l’épaisseur de fraisage, sélectionner le type et épaisseur de la mèche utilisée.

Cliquer sur Z touche pour que la fraiseuse mesure la hauteur automatiquement. Il se met au point zéro de l’axe Z.

Avant de commencer à fraiser, vérifier la qualité des Shapertapes en déplaçant la machine sur le plan de travail.

Quand tout est bien vérifié, on peut commencer à fraiser mais avant cela, mettre les lunettes de protection et installer l’aspirateur en insérant l’adapteur d’aspirateur au pot.

Penser à régler la vitesse de fraisage si nécessaire, puis allumer la fraiseuse et commencer à fraiser sur le bouton vert.

Ne jamais au grand jamais changer la fraise en laissant la fraiseuse branchée.

Pour graver, la vitesse idéale est 3, mais pour le fraisage l’idéal est une vitesse réglée à 5. C’est indiqué en fonction du son, malheureusement compliqué pour moi.

Il est possible de raiser à une certaine distance décalée, soit à l’intérieur ou à l’extérieur du tracé, par exemple à 1-2 mm de décalage du tracé, attention au matériau car risque d’être abimé si le contour est trop fin, ça pourrait se briser facilement pendant le fraisage si c’est du bois.
Si on souhaite extraire une partie, la forme contournée, c’est conseillé de fraiser, pas toute l’épaisseur directement, petit à petit, de couche en couche pour éviter tous risques de destruction de la mèche ou de provocation de feu.

De plus en perçant trop profondément, ça laisserait trop de débris, ce qui ralentirait la mèche ou la bloquerait et l’endommagerait.

Voilà, j’ai à peu près tout dit, j’ai bien compris comment fonctionne la machine Shaper Origin, mais je ne l’ai pas encore utilisée. J’aimerais l’essayer une fois pour créer un prototype, peut-être pour le projet final, à taille réelle.

## FORMATION SHAPER ORIGIN 1ER DECEMBRE ##

Revenons à cette formation de Shaper Origin du 1er décembre.

Axel nous a expliqué et montré tout comme ce que les tutoriels ont expliqué.

C’était plus clair en réel, il y avait des conseils utiles comme ne pas gaspiller les ShaperTapes.

Le fablab n’a pas encore un aspirateur adapté à la machine Origin. Il est en commande, en cours de livraison, je n’en sais pas plus.

La formation était courte mais intéressante.

J’essayerai bientôt cette machine.

