# PROJET FINAL #

# PARTIE 1 : OBJET FINAL #

## PLANS ET COUPE ##

![git](../docs/images/PLANS_CHAISE_FINALE.jpg)

## PLANS DES PIECES ##

![git](../docs/images/PLANS_PIECES_CHAISE_FINALE.jpg)

## RESULTAT FINAL ##

![git](../docs/images/RESULTAT_FINAL.jpg)

Mon objet final est qu’un simple siège long comportant que 6 pièces, inspiré des lignes de la Endless Chair de Dirk vander Kooij, je me suis inspiré des lignes ou formes géométriques des pieds troués de la chaise. Dans mon objet final, la plus grande inspiration parmi d’autres est les pieds percés comme l’Endless.

Le repose-pied et la pièce servant de support des pieds arrière ont une sorte d’arc, du bas vers le milieu, au centre de la pièce. C’est aussi l’un des gros éléments inspirés de la chaise de Dirk Vander Kooij. Le dossier de mon siège long est courbé du haut, tout comme l’Endless.

Ma chaise comporte beaucoup d’éléments inspirés, c’est le mot « inspiration » que j’ai choisi parmi les 5 mots proposés, c’était celui dont ça me parle le plus. Seuls la matière, le type et le fonction de la chaise ont modifiés.

C’était un défi de la faire en bois, ça comporte plus de restrictions créatives alors que le plastique est plus flexible et large dans la création des formes. J’avais normalement prévu de rendre mon siège long facilement montable et démontable mais l’assemblage a bien démontré que ce n’est pas facilement démontable ensuite. C’est trop serré.

Lors l’assemblage, une nouvelle inspiration m’est venu en tête, je l’ai monté de pièce en pièce à partir du bas vers le haut, tout comme le procédé d’impression de l’Endless Chair de couche en couche du bas vers le haut.

J’ai utilisé deux panneaux multiplex de 244x122x1,8 cm et le multiplex est comme une sorte de réutilisation de bois multiples, comme le plastique de l’Endless Chair qui vient de multiples objets en plastique, ça pourrait aussi être une inspiration.


# PARTIE 2 : RECHERCHES ET PROTOTYPES #

![git](../docs/images/PHOTO_CHAISE_1.jpg)

Une chaise à bascule, appelé Endless Flow de l’artiste Designer Dirk Vander Kooij, cette chaise a été créé en 2011 au centre d’innovation et de Design au Grand Hornu à Hainaut en Belgique. Basée de matériaux en plastique recyclé et réutilisé à l’imprimante 3D

Le designer, Dirk Vander Kooij, d’origine Hollandaise a été diplômé à l’académie du design d’Eindhoven. Il travaille surtout au design mobilier basé de matière en plastique recyclé. Il a conquis et programmé une vieille machine robotique venant d’une usine chinoise pour en faire un appareil d’impression 3D géant automatisé. L’artiste n’avait qu’à programmer et régler la machine pour créer une forme qu’il souhaite et la machine fait le travail en fondant et éjectant la matière plastique fondu de couche en couche.

En 2011, il a conçu la chaise Endless Flow, il y en a plusieurs de formes différentes, cette œuvre a été accablée gagnante au Dutch Design Award.

![git](../docs/images/PHOTO_CHAISE_2.jpg)
 
Voici une autre chaise d’une forme différente, appelée « the chubby chair », conçue à un peu près la même méthode mais plus rapidement et simplement. Les couches sont plus épaisses et sont soutenues par les supports pour les tenir sans qu’elles s’écoulent avant de se solidifier pour l’assemblage.

Pourquoi j’ai choisi cette œuvre, parce que j’aime beaucoup l’idée de réutiliser les déchets en plastique en les recyclant pour leur donner une nouvelle vie et une nouvelle utilité au quotidien avec la technologie d’impression 3D au lieu de les jeter inutilement.
Cette méthode pourrait répondre à la problématique actuelle de la crise des déchets en plastique dans le monde. L’impression 3D m’a toujours fasciné depuis son invention, avec cette technologie, on peut créer plein de formes qui étaient presque impossible à concevoir, la possibilité est plus large. J’aimerais me former pour utiliser cette technologie.

Au début, j’avoue que je n’aimais pas trop cette chaise à bascule, c’était juste le procédé de création qui m’intéressait mais après avoir fait des recherches sur Dirk Vander Kooij et ses œuvres, ses mobiliers en plastique m’ont complètement conquis, je les adore ! Surtout la table ci-dessous.

![git](../docs/images/PHOTO_TABLE.jpg)
 
D’habitude, je déteste les mobiliers en plastique, c’est vulgaire, ça me dégoutait mais là Dirk m’a prouvé qu’on peut les rendre beaux.
La table a été conçu avec des éléments de la Chubby Chair, des vinyles, d’autres objets en plastique et on les a tout fondu et versé dans un support creux d’une forme spécifique avant de l’enlever une fois la matière solidifiée pour l’étape de l’assemblage. J’adore le résultat visuel, on dirait du marbre mais non c’est du plastique !

Regarder [vidéo](https://www.youtube.com/watch?v=92SLUXnzfmY&ab_channel=FlindersDesign) pour voir le procédé d'impression de l'Endless Chair.

## MON SIEGE IMPRESSION 3D ##

![git](../docs/images/PHOTO_CHAISE_6.jpg)

## CHOIX DES MOTS ##

Pour le projet final, il nous faut se référer notre objet choisi au musée ADAM avec ces cinq mots :

-	Inspiration
-	Référence
-	Extension
-	Influence
-	Hommage

Pour le projet final, je dois choisir un ou deux de ces cinq mots qui me parlent le plus. Je dois aussi expliquer pourquoi avoir choisi ces mots et trouver les différentes définitions au dictionnaire.

J’avais choisi deux mots : Inspiration et Extension, c’est ce qui m’inspire le plus par rapport au nouvel objet imaginé dans ma tête. J’avais imaginé créer une chaise longue extérieure en bois. Je me suis inspiré des lignes comme des couches d’assemblage en bois. Et l’extension en rapport à l’extension du siège à bascule en chaise longue.

Je vous montrai le modèle 3D de ce que j’avais imaginé.

Revenons aux définitions de ces deux mots.

## Definitions ##

Venant de LAROUSSE

Inspiration :

-	Phase respiratoire dans laquelle l’air atmosphérique, riche en oxygène, pénètre dans les poumons.
-	Mouvement intérieur, impulsion qui porte à faire, à suggérer ou à conseiller quelque action : suivre son inspiration.
-	Enthousiasme, souffle créateur qui anime l’écrivain, l’artiste, le chercheur : chercher l’inspiration.
-	Ce qui est ainsi inspiré : de géniales inspirations.

Ces quatre définitions, les quatre sens du mot sont les plus connus du Larousse. Les deux dernières définitions me parlent le plus. Voyons les définitions des autres dictionnaires.

## MES PREMIERS PROTOTYPES ##

Voici ma première recherche pour mon objet à créer, j’ai utilisé deux mots : l’inspiration et l’extension. Je me suis inspiré des trous sous l’assise mais cette fois, je ne faisais pas de courbes mais des lignes droites et pour donner plus de confort à l’assise, j’étends le siège.

Voici quelques croquis ci-dessous

![git](../docs/images/CROQUIS_1ER_PROTOTYPES_1.jpg)
![git](../docs/images/CROQUIS_1ER_PROTOTYPES_2.jpg)

La partie principale, l’assise en bois est soutenue par une deuxième partie en aluminium, sans cela l’assise s’effondrera.

Voici un aperçu en 3D

![git](../docs/images/SIEGE_LONG_3D_1.jpg)
![git](../docs/images/SIEGE_LONG_3D_5.jpg)
![git](../docs/images/SIEGE_LONG_3D_6.jpg)

Au départ, je ne savais pas qu’il fallait créer un siège à la taille réelle, alors là, c’est super compliqué de créer ce modèle 3D.

J’ai créé ce siège de la même méthode de conception, sur fusion 360, que le siège en impression 3D que j’avais déjà créé, mais cette fois c’est avec des lignes plus droites, des angles droits, ça donne une ligne plus sobre, simple mais élégant.

Cela reste quand même compliqué à créer avec l’assemblage en bois surtout cela risque de prendre beaucoup de temps, le temps presse …

J’avais déjà conçu un autre siège, inspiré de mes croquis ci-dessous

![git](../docs/images/CROQUIS_1ER_PROTOTYPES_3.jpg)
![git](../docs/images/CROQUIS_1ER_PROTOTYPES_4.jpg)
![git](../docs/images/CROQUIS_1ER_PROTOTYPES_5.jpg)

Ce siège est conçu de la même méthode du premier prototype, même matière, deux parties, l’une en aluminium et l’autre en bois. Mais cette fois avec des courbes, ce qui rend la création de ce siège plus aisée mais surtout très couteuse. Voici un aperçu en 3D.

![git](../docs/images/SIEGE_LONG_3D_2.jpg)
![git](../docs/images/SIEGE_LONG_3D_3.jpg)
![git](../docs/images/SIEGE_LONG_3D_4.jpg)

La conception de cette chaise longue et courbe est bâclée car l’assise ressemble à une langue. Ce n’est pas ce que je voulais, je voulais une ligne continue unie sans forme de fesses. Il s’agirait d’un problème technique avec le programme, je n’arrivais pas à comprendre, c’est pour cette raison que ça a été bâclé.

Je laisse tout ça de côté car ce sont deux prototypes trop compliqués à construire, de toutes façons, vu le temps qu’il me reste, je ne parviendrai pas à mes fins.

Je vais concevoir un siège en bois simplement en m’inspirant des lignes des couches de la Endless chair.

Là, j’ai décidé de retirer le mot extension car le mot inspiration suffit, je m’inspire aussi des courbes et des trous sous l’assise.

Avec ce que j’ai comme idées en tête, la machine laser suffira peut-être pour concevoir ce modèle en bois.

Je vous montrai le modèle 3D que j’ai crée.

## DESCRIPTION DU TROISIEME PROTOTYPE ##

![git](../docs/images/CAPTURE_PROTOTYPE_3_3.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_3_1.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_3_2.JPG)

Je me suis inspiré des lignes courbes et des trous sous l’assise, je m’inspire de l’armature, mais cette fois, je le ferai en bois multiplex. Il s’agirait d’un assemblage en plusieurs couches de multiplex qui seront assemblés par des tubes, sois en bois ou soit en inox pour un système de vissage plus facilement démontable sans trop abîmer.

Le siège est fixe, mais un des étudiants m’a suggéré de faire un système de dossier rabattable et inclinable à notre convenance selon le confort souhaité.

J’ai apprécié l’idée que j’ai trouvé excellente, je peux remercier cette étudiante, je n’ai hélas pas demandé son prénom. Après la conception du siège long fixe, j’ai réfléchi afin de créer un autre avec un système rabattable, c’était un défi, mais pas impossible.

On m’a mis en garde en ce qui concerne le poids du siège qui risque d’être trop lourd. Je dois penser à ça et à ne pas trop gaspiller le bois. Alors que le troisième prototype exigerait énormément de panneaux multiplex.

Ça m’a permis de prendre conscience du gaspillage énorme, qui de plus couterait super cher, ce ne serait pas un réel succès.

La conception de ce troisième prototype est simple, mais prend beaucoup de temps, je dois en effet tenir compte du temps de conception.

## LE 2 DECEMBRE ##

J’avais réservé une place à 13h pour un découpage laser, j’allume la machine, ouvre la vanne d’air, allume les extracteurs de fumée.

J’ouvre l’interface de Lasersaur, insère ma clé USB, sélectionne mon fichier SVG.

Mon fichier SVG comporte deux couleurs différentes, la première est pour découper les plus petits points, car si je découpe d’abord les contours autour de ces petits points, ils vont tomber à travers les grilles sans que le laser puisse les découper, ce serait un échec, j’avais prévu ça.

J’utilise du carton gris de 2 mm, je clique sur Home pour que le curseur se dirige vers son point d’origine et je vérifie le mouvement autour de mon fichier SVG avec Move.

Etrangement le curseur dépassait de 5 cm en bas à gauche du carton gris, pourtant mon fichier SVG fait 65 cm sur 50 cm comme le carton gris que j’ai placé du mieux possible.

Ça dépasse encore légèrement, je laisse comme ça et on verra.

Je règle le 1er passage à l’intensité de 60% à la vitesse de 2000 mm/min, j’avais mal compris, je pensais que plus je mets plus c’est rapide, en fait non, c’est l’inverse.

Le deuxième passage a le même réglage, je ferme le couvercle, le statut est OK et je commence le découpage.

Le premier passage s’est bien passé, mais le problème commence au deuxième, il y a eu un réel gros problème de décalage qui s’aggrave de plus en plus, je suis dégouté, j’ai dû arrêter le découpage, étrangement le laser n’a même pas traversé le carton gris, il manquait peut-être de puissance ou alors c’était toujours trop rapide.

Je montre le problème aux assistantes, leur demande si ce problème est normal ou pas. Ce n’était pas normal, la machine a un souci, mais je ne sais pas lequel.

Il doit y avoir quelque chose qui dérègle les rails, ça faisait bouger le curseur petit à petit.

Les assistantes ont dû faire appel à un technicien et ont tenté nettoyer la machine, en vain.

Une autre élève, Manon, si je me souviens bien, a voulu faire le découpage d’un bois multiplex de 9mm environ, elle rencontre le même problème que moi.

Le temps passe super vite, j’attend toujours que la machine soit réparée afin que je puisse découper mon prototype.

J’avais deux prototypes à essayer, mais malheureusement, la machine a rendu l’âme.

Ce jour-là, on ne pouvait rien faire, je suis rentré sans rien, j’étais très frustré et déçu, je voulais tellement voir le résultat de l’assemblage.

J’ai pensé revenir le lendemain à 12h réessayer au cas où la machine fonctionnerait. J’ai proposé à Manon de prendre une place avec moi le lendemain à 12h car il y’avait plus de place.

La journée était déjà terminée.

## LE 3 DECEMBRE ##

C’était le jour de la correction des travaux de chaque élève, il faut présenter notre avancée et nos prototypes.

C’était présentiel, je n’avais qu’un modèle 3D et des plans à présenter, j’aurais aimé que la machine fonctionne, j’étais déçu, j’espérais ne pas être pénalisé. Au moins j’avais un modèle 3D assez clair.

L’attente fût longue, il y avait du retard, mon tour devait avoir lieu à 15h50 mais je suis passé vers 17h, c’était un problème pour ma traductrice qui a dû attendre.

Ma présentation n’a été ni positive ni négative. J’avais montré le modèle 3D et j’ai reçu les commentaires du professeur Levy et de ses deux assistantes :

-	Le poids de ma chaise longue paraît trop lourd à déplacer, il ne doit pas excéder 7kg. Je dois voir pour l’alléger.
-	Il y a trop de matière extrudée, ce n’est pas très écolo, c’est du gaspillage, je dois être économe !
-	Il faut que je simplifie pour gagner du temps, je dois prendre les chemins les plus courts quand je réalise un modèle.
-	Faire plusieurs essais pour retirer la matière sous l’assise car trop chargé et presque inutile.
-	Je peux prévoir des accoudoirs amovibles à condition qu’ils soient plus légers.
-	Dans mon modèle, m’inspirer de celui qu’une des assistantes m’a montré : assemblage des pièces reliées par une pièce unique, mais c’était ce que
j’avais fait dans l’assemblage de mon modèle.

J’ai bien compris ce que je dois faire, je dois être malin, construire un objet facile et intelligent, c’est la meilleure façon de gagner du temps.

C’est dommage que la machine laser soit tombé en panne au pire moment, sinon j’aurais eu deux ou trois modèles, maquettes très claires, ce qui m’aurait permis de mieux visualiser l’ensemble et voir l’étape d’assemblage.

Ça aurait pu être efficace à la présentation.

## DESCRIPTION DU QUATRIEME PROTOTYPE ##

![git](../docs/images/CAPTURE_PROTOTYPE_4_3.JPG)

Ce siège long est aussi en bois, mais est équipé d’un système rabattable, son design est un peu différent du troisième prototype, ses lignes sont plus droites avec des extrémités arrondies pour un assemblage uni.

Mon idée est un système rabattable avec une structure en L, mais je n’ai pas pu faire des essais vu les problèmes de la machine Laser. Donc à la place, j’ai fait un modèle 3D pour voir ce que ça donne. Voir photo ci-dessous

![git](../docs/images/CAPTURE_PROTOTYPE_4_1.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_4_2.JPG)

Ça reste malgré tout très massif, néanmoins, il y a beaucoup moins de gaspillage, mais pas encore assez intelligent, je dois récupérer le maximum de matériaux afin d’éviter le gaspillage inutile.

Le dossier et le repose pieds sont reliés par une structure en L mais souple, pour faire baisser le dossier et relever le repose pieds. Il faut aller derrière le dossier afin de régler l’inclinaison. Je dois encore réfléchir afin de ne plus devoir se lever pour l’inclinaison du dossier.

Je dois imaginer un cinquième prototype avec à peu près le même système mais réglable en restant assis.

Je dois aussi réfléchir à un assemblage avec des pièces plus droites et des extrémités arrondies, éviter trop d’extractions de matériaux. Je vais en dessiner pour mieux visualiser comment faire.

## DESCRIPTION DU 5E PROTOTYPE ##

Voici quelques croquis de recherche, j’ai suivi l’inspiration des lignes, des couches du siège rabattable, je fais toujours un siège rabattable. 

J’essaie toujours, ce n’est pas simple car à chaque fois je change et essaie une autre conception.

A chaque nouveau design du siège, il y a un problème, son poids et sa masse qui sont assez importants.

Le 5e prototype en question ci-dessous.

J’ai d’abord dessiné les pièces d’assemblage avec Autocad. Voir capture d’écran ci-dessous.

![git](../docs/images/CAPTURE_PROTOTYPE_5_3.JPG)

J’ai voulu faire une maquette avec la découpe Laser, pour voir ce que ça donne. J’ai finalement changé d’avis après avoir fait un modèle 3D avec Sketchup que je trouve assez Laid et encore trop massif. Voir photo ci-dessous.

![git](../docs/images/CAPTURE_PROTOTYPE_5_1.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_5_2.JPG)

Les pieds ne me dérangent pas, leurs formes correspondent à l’inspiration des formes des trous sous l’assise du siège choisi. Ce qui me gêne le plus, ce sont les 2 supports qui tiennent le dossier rabattable, ce sont des éléments en trop, ou bien qui ne sont pas assez réfléchis, peut-être rendre leur design cohérent aux formes des pieds.

Le dossier et le repose-pieds ne correspondent pas assez au design des pieds du siège mais j’ai fait en sorte que le dossier et le repose-pieds soient droits pour permettre d’économiser les panneaux multiplex en rangeant et découpant les rangées des pièces du dossier et du repose-pieds.

J’ai beaucoup réfléchi pour améliorer le design des supports du dossier rabattable, je me suis dit que peut-être un seul support suffira et améliorera peut-être la beauté du design.

Avec la conception d’un seul support, j’ai essayé de m’imaginer un seul pied à l’arrière au milieu du dossier avec le support au milieu aussi, deux pieds à l’avant bien sûr pour une question de stabilité.

Je me suis dit que c’est peut-être une meilleure solution pour améliorer le siège en le rendant un peu plus léger.

Il me reste qu’à essayer en faisant des croquis.

## DESCRIPTION DU 6E PROTOTYPE ##

Voici quelques croquis de recherches, les formes géométriques triangulaires semblent beaucoup m’obséder, mais c’est l’idée, je me suis inspiré de la forme géométrique des trous sous l’assise.

![git](../docs/images/CROQUIS_PROTOTYPE_6.jpg)
![git](../docs/images/CROQUIS_PROTOTYPE_6_2.jpg)
![git](../docs/images/CROQUIS_PROTOTYPE_6_3.jpg)

Il s’agit d’un siège long à trois pieds, les pieds sont inspirés des trous sous l’assisse de l’objet choisi.

Ce siège n°6 est aussi rabattable, sans accoudoir, pour les raisons du poids du siège, j’ai donc préféré retirer les accoudoirs.

J’ai dessiné le 6e prototype avec Autocad, il ressemble beaucoup au 5e, mais cette fois, je n’ai mis que trois pieds au lieu de quatre, afin que le pied arrière résiste mieux au poids, il faut qu’il soit plus massif et solide.

Voici les pièces utilisées pour l’assemblage.

![git](../docs/images/CAPTURE_PROTOTYPE_6_4.JPG)

Une fois le dessin terminé, j’importe sur Illustrator pour l’enregistrer au SVG pour le découpage Laser, j’ai même fait un modèle 3D sur Sketchup, voir photo ci-dessous.

![git](../docs/images/CAPTURE_PROTOTYPE_6_3.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_6_2.JPG)

Cette fois, le design fonctionne mieux que celui du 5e prototype, mais je crains que ce soit encore trop massif. J’aime beaucoup l’idée des trois pieds, ça donne bien, mais il faudra voir la résistance et la stabilité quand je ferai la maquette à l’aide du découpage Laser.

## LE 16 DECEMBRE ##

J’avais réservé une place pour l’utilisation de la machine à découpe Laser au fablab ULB. J’avais déjà préparé le fichier SVG, il était prêt dans la clé USB.

Je le sélectionne et l’importe sur l’interface du Lasersaur. Comme j’utilise du carton gris de 2 mm, Clémentine m’a conseillé F800/60% pour que ça se découpe bien.

Il y a deux passages, le premier en noir pour les plus petits points qui vont directement tomber, les autres tracés en rouge, tous les deux au même réglage de découpe.


Après avoir tout vérifié, vanne, filtres, extracteurs, etc., je commence la découpe. Le premier passage se passe bien, malheureusement, le deuxième passage, ça ne va pas.

C’était encore ce problème de décalage ! Je ne comprenais pas, j’avais demandé de l’aide à Margaux, elle ne comprenait pas non plus, mais elle m’avait suggéré d’écarter un peu l’espace entre les pièces car j’avais laissé trop peu d’espace. Le découpage ne laissait qu’un espace très mince entre les deux pièces, ce qui les faisaient bouger entre eux, c’est ce qu’avait pensé Margaux.

Je suis le conseil de Margaux, je retourne sur Autocad écarter l’espace entre les pièces avant de l’enregistrer et exporter vers Illustrator pour enregistrer sous SVG. 

Je réessaie en gardant le même panneau de carton gris 2mm, heureusement, j’ai annulé manuellement le précèdent découpage en voyant les défauts de découpe. Le panneau avait encore assez de place pour le deuxième découpage avec le nouveau fichier modifié.

Je commence la découpe, je croise les doigts et j’attends.

J’attends de voir si le problème persiste ou pas.

Quelques minutes après le second passage, je constate que ça fonctionne mieux qu’avant. Là, je pense avoir compris pourquoi il y avait un dérèglement au découpage précédent.

Les bâtonnets trop fins pouvaient traverser les grilles, ils se soulevaient et touchaient parfois le curseur du Laser. Les bâtonnets se lèvent et restent coincés sur les grilles, sans tomber dans le fond.

A mon avis, c’est à ce moment là que se provoquent des collisions entre le curseur en mouvement et le bâtonnet coincé.

J’ai surveillé le second découpage, mis une fois sur PAUSE pour ouvrir la boîte pour retirer ou remettre la pièce coincée.

Cette fois enfin, le découpage s’est bien passé, c’est le premier depuis le 2 décembre, la première découpe au laser avec du carton gris.

Je commence à trier les pièces découpées pour commencer à assembler, c’est amusant.

C’est assez dur d’insérer le tube en bois à travers les trous assez étroits, il faut forcer mais prudemment, c’est délicat. Je dois tourner le tube pour l’enfoncer mais ça peut abîmer les pièces.

J’ai néanmoins réussi à assembler la moitié, ce qui est déjà bien, c’est ce qui m’a permis d’essayer le système d’inclinaison. Et ça fonctionne bien, sauf que j’ai brisé le support du dossier rabattable.

Je peste un peu mais il fallait s’y attendre avec une maquette à l’échelle 1/10.

Pendant le deuxième découpage, j’avais préparé deux fichiers SVG de tracés à l’échelle 1/5 au cas ou ça ne fonctionnerait pas avec l’échelle 1/10

![git](../docs/images/CAPTURE_PROTOTYPE_6_5.JPG)

Même si ça a marché, étant donné que personne n’utilisait la machine en dehors de mes heures de réservation, j’en ai profité pour découper ceux à l’échelle 1/5 avec des panneaux de carton gris, cette fois plus épais, 3 mm, ce qui correspond à l’épaisseur du bois multiplex de 1,5 cm à l’échelle 1/5.

Ce découpage nécessitait deux panneaux de 65/50 cm. J’ai mis les réglages à F800/65%.

Ça coupe super bien, cette échelle se découpe plus facilement et plus proprement que l’échelle 1/10.

Il me reste qu’à l’assembler chez moi, j’ai hâte de voir le résultat final.

Ce modèle n°6 me plait beaucoup, mais je crains quand même pour sa stabilité et son poids. Il reste encore trop chargé, mais le système d’inclinaison est mieux évolué que les autres prototypes. Je vais simplifier encore plus.

Ce siège peut avoir le même système d’inclinaison, mais ne doit comporter que très peu de pièces à réaliser, des pièces très efficaces à assembler, car là, c’est beaucoup trop de pièces à réaliser à l’échelle 1/1.

Cela demanderait beaucoup de temps, de patience. Beaucoup de panneaux vont être utilisés donc ça me couterait cher.

Je dois encore réfléchir à un autre modèle, peut-être avec trois pieds, avec un système rabattable mais beaucoup simplifié, avec beaucoup moins de pièces.

## ZOOM SUR LE SYSTEME DE VISSAGE ##

Je vais utiliser soit un tube en inox soit en bois, je penche plutôt sur celui en inox car celui-ci frotte moins avec le bois, que le bois avec le bois.

De plus l’inox est plus résistant, le tube est creux, donc plus léger, on peut faire un système de vissage, je ne connais pas le mot précis.

On vissera chaque coté avec des écrous, ça permettrait d’assembler les couches et les désassembler comme je veux.

Ci-dessous, un petit aperçu du système de vissage.

![git](../docs/images/DETAIL_1.jpg)

Ce n’est qu’une solution parmi d’autres. Il y en a sûrement d’autres meilleures.

## JEUDI 17 DECEMBRE 2020 ##

C’est le jour du pré jury, je suis arrivé à 13h30, mon tour de passage est à 14h. C’était la pause déjeuner des membres du jury. Et c’est drôle, parmi les membres du jury, il y avait une de mes anciennes collègues de l’Académie Royale des Beaux-Arts, Alice, on s’est dit bonjour, elle était jury invitée, j’ai failli ne pas la reconnaitre.

Bon, passons ces retrouvailles, la traductrice Cécile vient d’arriver, elle avait besoin de quelques préparations et de savoir ce que j’allais présenter. Je lui ai expliqué et fait un résumé de l’évolution de ma maquette depuis le début.

A 14h30 avec un peu de retard, mon tour est arrivé, je me rends avec mes maquettes vers la salle des jurys.

Je présente mes maquettes à la table, m’installe derrière l’ordinateur et commence ma présentation, j’explique l’évolution, déjà relaté dans GIT, un bref résumé.

Voici la maquette

![git](../docs/images/MAQUETTE_13.jpg)
![git](../docs/images/MAQUETTE_14.jpg)
![git](../docs/images/MAQUETTE_17.jpg)

Je leur ai expliqué que ma maquette rencontre des problèmes de stabilité avec les trois pieds, j’en étais bien conscient quand j’avais réalisé et assemblé la maquette.

J’ai proposé d’inverser les pieds, deux à l’arrière et un à l’avant. J’ai également signalé que j’étais dans l’orange, à cause de petits retards, en cause des pannes de la machine Laser et qu’il me reste encore beaucoup de pain sur la planche afin de trouver d’autres solutions pour alléger le siège.
Ma présentation terminée, j’écoute les avis des membres du jury, mon ancienne collègue, Alice, est la première à réagir. Elle dit que peut-être pour répondre au problème de stabilité dû aux trois pieds, il faudrait peut-être imaginer un système de pieds connectés au dossier rabattable.
Ce seraient à la fois les pieds et les supports du dossier rabattable. J’y avais pensé aussi, et peut-être simplifier avec des panneaux, tout comme ce que pense Victor Levy.

Les commentaires de Victor Levy.

-	Jamais et presque interdit de créer une chaise avec trois pieds, ça provoque trop d’accidents. Mais après avoir fait des recherches sur internet, il a dit que les chaises à trois pieds existent mais sont interdites dans les lieux de travail, mais peuvent être dans les domiciles. Donc je peux garder l’idée des trois pieds, mais améliorer la stabilité.

-	Les tubes en inox qui assemblent les pièces pourraient comporter des risques de frottements entre les pièces lorsqu’on baisse ou lève le dossier et le repose-pied. C’est tres compliqué, il faudrait a la fois bien serrer pour que le siège ne bouge pas trop, mais rendrait le siège plus difficilement rabattable, c’est trop compliqué.

-	Surtout trop d’heures de travail, il faudrait que je passe des nuits au Fablab, beaucoup trop de pièces. Il me faut simplifier, avec des panneaux qui s’assemblent entre eux. Il a dit d’être pur et simple mais efficace.

-	Il ignorait jusqu’à ce que je lui montre la maquette que celle-ci est mécanisée. Il est tombé sous le charme, il a dit que mon design a une bonne logique par rapport au siège d’origine avec ses formes géométriques et son procédé de construction avec des couches. Il comprend mieux mon entêtement. Il ne veut pas gâcher mon travail.

-	En même temps, il dit avoir beaucoup de doutes et d’inquiétudes quant à ma suite, que je dois encore simplifier mais en même temps, il aime bien ce que j’ai réalisé. Il hésite.

Les commentaires de la dame dont je ne sais plus son nom.

-	Elle m’a suggéré d’utiliser une poupée ou un mannequin en bois afin de tester la stabilité et l’assise du siège. Il faudra que je demande à mes nièces de me prêter leurs poupées.

Les commentaires des deux assistantes.

-	Leurs commentaires sont presque identiques à ceux de Victor Levy, simplifier et alléger ! Mon siège est trop massif.

-	J’aurai beaucoup de problèmes de frottement entre les pièces à chaque fois que je léve ou baisse le dossier et repose-pied. Ça ferait grincer le système. J’ai eu cet insupportable sensation de grincement en insérant le tube en bois à travers les trous en tournant.

-	Elles me disent aussi de faire attention au temps, de bien organiser chaque étape de travail, quelle machine utiliser…

-	Que je dois absolument simplifier.

Les jurys ont bien conscience que ce n’est pas facile de créer un siège, à la fois en bois, rabattable, tout en suivant l’inspiration des formes géométriques et le procédé de création du siège choisi au musée ADAM.

Mon jury est terminé, ça a l’air d’aller, on a beaucoup discuté entre nous. C’était passionnant, je suis satisfait.

## DESCRIPTION DU 7e PROTOTYPE ##

Le 7e prototype est beaucoup plus léger mais malheureusement moins intéressante techniquement car il n’est plus rabattable.

Le système rabattable rend le travail plus compliqué et plus long. C’est plus compliqué à réaliser, je n’ai pas beaucoup de temps donc je suis passé au plan B.

J’ai enlevé le système rabattable, ce qui rend le design beaucoup plus fin et demande clairement moins de travail, et beaucoup moins de pièces.

Voici les modèles 3D

![git](../docs/images/CAPTURE_PROTOTYPE_7_1.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_7_2.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_7_3.JPG)

Le 7e prototype est plutôt un siège classique pour s’asseoir. L’inspiration des pieds et ses formes géométriques est présente mais le procédé de construction est moins présent à cause de la suppression des lignes de couche en couche comme les précédents prototypes.

C’est compliqué de garder l’inspiration de ces lignes à cause des panneaux, j’avais pensé utiliser des planchers extérieurs en ipé, en profilé rainurée qui rend le visuel l’impression que c’est fait de couche en couche, mais c’est trop lourd et fragile car je dois les assembler verticalement, il y a un risque que ça cède sous le poids de la personne assise. Alors j’ai laissé tomber cette idée, peut-être reviendrais-je sur cette idée après avoir vu les professeurs pendant la correction.

A la place de ces planches, j’ai simplement utilisé des panneaux multiplex avec des sommets du dossier et repose-pieds arrondis comme l’Endless chair, c’est une nouvelle inspiration. Ça rend le design plus souple, moins rigide, moins brut. C’est mon avis.

Je dois encore travailler les pieds car je trouve que c’est trop tiré par les cheveux. J’hésite encore.

Pour les matériaux de ce 7e prototype, j’avais d’abord penché pour du OSB pour les pieds et du multiplex pour l’assise, le dossier et repose-pieds, ou inversement.

Je savais que l’utilisation du OSB rend les pieds plus fragiles. Certes, j’avais cherché et regardé sur le net afin de savoir s’il existe des sièges en OSB. Ça existe, mais la plupart ont des pieds plus épais, massif, plus résistants alors que ceux du 7e prototype sont assez fins.
Même si ça peut fonctionner, il va falloir trouver de bons panneaux bien compressés pour une bonne qualité de découpage, sinon il y aurait trop d’éclats dû au découpage.

L’avantage du panneau OSB est son prix et sa légèreté, mais la qualité n’est pas toujours présente.

J’ai donc pensé n’utiliser que du multiplex, c’est plus cher, mais la qualité est là, c’est beaucoup plus solide et plus élégant avec ses lignes. Alors là j’ai fait tilt ! Peut-être pourrais-je faire référence des lignes du multiplex à celles des lignes de l’Endless chair ! C’est peut-être n’importe quoi, mais je pense en discuter avec les professeurs et voir ce qu’ils en penseront.

L’assemblage de ce prototype se fera à la colle, ce ne sera hélas plus démontable comme les précédents 4e,5e et 6e prototypes. C’est le gros inconvénient du 7e prototype.

J’ai choisi de faire plus simple et de gagner du temps.

Peut-être qu’en ayant du temps, je pourrais imaginer un prototype qui ne demande qu’un assemblage sans colle, juste des vis. 

Ce prototype n’aura que 15 pièces à réaliser, à fraiser.

![git](../docs/images/CAPTURE_PROTOTYPE_7_4.JPG)

J’utiliserais seulement le Sharper Origin pour faire mon travail.

Il ne me faudrait qu’un panneau multiplex d’une dimension de 244 x 122 cm pour fraiser ces 15 pièces, c’est largement suffisant, c’est bien juste.
Je pense acheter ce panneau multiplex de 15 mm chez Hubo, ça coute dans les 50 euros, plus le prix de la colle à bois.

Je ne sais pas encore laquelle mais je demanderai conseil à mon père qui travaille le bois depuis de nombreuses années. Il sera très utile à mes fins.
Je diviserais ce panneau en 4 pour avoir deux panneaux 61 x 144 cm et deux panneaux de 61 x 100 cm, c’est assez lourd et encombrant, je vais devoir réfléchir au moyen de transport, en train ce n’est pas évident !

J’ai un peu peur de n’avoir assez de temps pour faire toutes ces pièces. J’ai donc pensé à un autre prototype afin de réduire le nombre de pièces à créer.

Je vous présente mon 8e prototype ci-dessous.

## DESCRIPTION DU 8E PROTOTYPE ##

Le 8e prototype ressemble beaucoup au 7e mais cette fois, il s’agit d’une chaise longue et basse. Ça rend le design plus épuré et beaucoup plus élégant je trouve, il me faut le tester en maquette afin de voir si ça fonctionne avec trois pieds.

![git](../docs/images/CAPTURE_PROTOTYPE_8_1.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_8_2.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_8_3.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_8_4.JPG)

En effet, j’ai encore utilisé ce système, mais cette fois à l’inverse, deux pieds à l’arrière et un à l’avant, cela pourrait peut-être fonctionner car le poids de l’assise va vers l’arrière. Il y a beaucoup moins de charge à l’avant.

En utilisant ce système, j’ai encore réduit le nombre de pièces à réaliser, c’est un des avantages, visuellement il est encore plus léger. Je présenterai ces deux prototypes et peut-être encore deux autres aux professeurs.

Voici les pièces du prototype 8.1

![git](../docs/images/CAPTURE_PROTOTYPE_8_5.JPG)

J’utiliserais aussi un panneau multiplex pour la création de ce prototype, un panneau de 244 x 122 cm suffira pour créer les pièces, ce prototype ne possède que 8 pièces, c’est presque la moitié en moins que le 7e.

Mais je m’inquiète un peu quant à la fragilité des pieds vu qu’ils n’auront que 15 mm d’épaisseur, c’est fragile, peut-être se renforceront-ils avec l’assemblage, mais j’ai quand même un doute. Si ça ne va pas, je devrai peut-être utiliser du multiplex plus épais, 18 mm pour être sûr.

Mais il y a un petit problème concernant les assemblages, mon père m’a mis en garde, je n’y avais pas pensé. Le problème, c’est que lorsque je ferai des fraisages des mortaises qui permettront de renforcer l’assemblage, leurs coins seront arrondis, ça compliquera l’insertion des tenons et pourrait nuire l’assemblage.

Pour régler ce problème, je vais devoir extruder les coins arrondis manuellement avec un outil spécifique. 9a allongera un peu le travail, mais pas le choix, c’est primordial pour la réussite.

Je vais devoir en discuter avec les professeurs. Ce problème sera aussi présent pour le 7e prototype.

Je trouve quand même que l’assise du 8e prototype est très basse, ce qui rendrait difficile le fait de se relever, surtout pour les personnes sensibles des articulations.

Peut-être ne vais-je pas percer mais juste fraiser pour permettre un renforcement avec des vis en plus. Le bémol, c’est qu’on verrait les têtes de vis, ce serait moins beau. On peut les cacher en les recouvrant avec un produit spécial, un enduit spécial pour bois, mais les taches se verront quand même. Ça n’est pas trop grave mais ce serait dommage.

J’ai conçu un deuxième, le prototype 8.2, un siège long avec une assise plus haute pour mieux comparer le confort de l’assise avec le 8.1. Je ferai ces deux prototypes en maquette pour mieux comparer, voir leur résistance et leur stabilité avec les trois pieds.
Voici les plans du prototype 8.2

![git](../docs/images/CAPTURE_PROTOTYPE_8_6.JPG)

Comme le prototype 8.1, ce siège long ne demande que huit pièces à réaliser, mais nécessiterait plus d’un panneau multiplex.

## DESCRIPTION DU 9E PROTOTYPE ##

Le 9e prototype est le plus simple de tous les prototypes.

J’ai imaginé 3 sortes, l’un avec les pieds extrudés, l’autre avec des pieds pleins. Certes, celui aux pieds pleins rend plus massif mais il est très épuré.

La création de ces prototypes 9.1, 9.2, 9.3 est très rapide et efficace pour en faire des productions en série. Reste le problème des mortaises qui empêchent l’assemblage propre.

J’ai fait plusieurs modèles en 3D. Les voici ci-dessous

![git](../docs/images/CAPTURE_PROTOTYPE_9_1.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_9_2.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_9_3.JPG)

Un petit aperçu des plans des pièces

![git](../docs/images/CAPTURE_PROTOTYPE_9_4.JPG)

Autres prototypes 9 en 3D

![git](../docs/images/CAPTURE_PROTOTYPE_9_5.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_9_6.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_9_7.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_9_8.JPG)

Mais je préfère attendre et les voir en maquettes mercredi. J’ai au moins quatre découpages laser à faire.
Ces trois prototypes ne demandent que 6 pièces à réaliser chacun, c’est très peu, je crains que l’on me pénalise car au départ il y avait une grande différence de travail. Maintenant c’est un travail moindre, presque paresseux. Même si on m’a dit qu’être paresseux était plus efficace, je ne suis pas convaincu, je dois en parler à la correction pour être plus sûr au plus tôt.

## MERCREDI 13 JANVIER 2021 ##

J’avais réservé à ce jour une machine laser pour tout l’après-midi. J’avais prévu de découper au laser les 7e, 8e, 9e prototypes dont plusieurs 8 et 9.
Le jour précédent, j’avais tout dessiné sur Autocad et exporté vers Illustrator pour en faire des fichiers SVG. Une fois sur place au fablab, j’ai directement utilisé la machine pour d’abord découper les prototypes 8, mes préférés.

J’ai utilisé des panneaux en carton gris de 3mm, j’ai mis la vitesse à 800 et la puissance à 35%. Ça a découpé toute la partie gauche du panneau mais n’a pas pu percer totalement la partie droite, je ne comprenais pas ce qu’il se passait, je craignais une nouvelle panne, j’ai dû découper manuellement les parties non percées.

Peut-être que le laser perdait de la puissance.

J’ai ensuite fait un autre découpage sur panneau de carton gris de 3 mm, je sélectionne les prototypes 9 au format SVG et je lance la découpe.
Le même problème persiste, j’ai dû refaire le reste manuellement. J’ai prévenu Gwen et Hélène du problème, elles ne trouvaient pas ça normal, elles ont pris contact avec Axel pour régler le problème, apparemment la machine est out pour la journée, je ne peux l’utiliser.

Mais au moins, j’ai pu obtenir les deux maquettes que je préfèrais. Normalement, la correction de mes travaux sera pour le lendemain. Mais il me reste peu de temps, j’avais encore quelques doutes concernant le choix de mes prototypes pour en faire mon objet définitif.

J’en ai profité pour présenter mes deux maquettes ci-dessous aux assistantes.

![git](../docs/images/MAQUETTE_1.jpg)
![git](../docs/images/MAQUETTE_2.jpg)
![git](../docs/images/MAQUETTE_3.jpg)
![git](../docs/images/MAQUETTE_4.jpg)
![git](../docs/images/MAQUETTE_5.jpg)

Je leur ai expliqué que j’ai choisi les pieds troués, la forme géométrique et la ligne de l’assise comme inspiration pour mon objet. Elles ont dit que la ligne du long siège est très intéressante et top, mais il n’a que 3 pieds, on m’a donc conseillé de mélanger le style du siège droit et le style du siège long, je devrai donc dessiner un nouveau prototype basé sur le style des deux chaises et bien penser à un siège ne demandant que 5-6 pièces afin de gagner du temps vu qu’il m’en restait très peu !

Une fois rentré chez moi, je me suis mis au travail, j’ai dessiné sur Autocad des prototypes 10 du siège droit et des prototypes 11 du siège long.
Ils ne demandent chacun que 6 pièces à réaliser. Voici les plans des pièces ci-dessous.

![git](../docs/images/CAPTURE_PROTOTYPE_10_7.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_10_8.JPG)

J’en ai fait des modèles 3D. Les voici ci-dessous.

![git](../docs/images/CAPTURE_PROTOTYPE_10_1.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_10_2.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_10_3.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_10_4.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_10_5.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_10_6.JPG)

Il ne restait qu’à les découper au Laser, j’espérais que la machine fonctionnerait le lendemain. En attendant, j’ai dessiné à l’aide d’Autocad un panneau de 244x122 cm pour savoir le nombre de panneaux je vais en avoir besoin pour la création de l’objet définitif.

Pour les sièges droits, il ne me faudrait qu’un panneau, c’est largement suffisant, j’ai vérifié sur Autocad en me basant sur les pièces du prototype 10.

Pour les sièges longs, c’est plus compliqué, il me faudrait deux panneaux de 244x122 cm. Je me suis dit que ça me couterait assez cher, mais je n’aurais pas le choix. Au fond de moi, j’espérais que Victor Levy choisirait le siège droit, le lendemain lors de la correction. Mais je ne le sens pas trop.

J’étais sûr qu’il sélectionnerait un des sièges longs. Le siège long était mon préféré, celui avec le repose-pieds et le support des pieds arrières courbés. Voir plans et 3D ci-dessous.

![git](../docs/images/CAPTURE_PROTOTYPE_10_7.JPG)
![git](../docs/images/CAPTURE_PROTOTYPE_10_1.JPG)

J’ai regardé les sites des magasins de bricolage afin de me faire une idée des prix des marchandises et comme je m’y attendais, ça allait me couter dans les 100 euros les deux panneaux multiplex de 244x122, voire plus.

Les prix varient selon les magasins, j’ai finalement décidé de les acheter chez Brico, le magasin d’en face du Fablab serait plus facile pour le transport.

Le lendemain sera décisif pour le choix définitif de mon objet.

## JEUDI 14 JANVIER 2021 ##

La machine laser habituelle est en panne, ça m’angoisse un peu, heureusement j’avais déjà préparé des modèles 3D au cas où. J’avais bien fait !
Axel était occupé à préparer et à régler la deuxième machine Laser, elle n’était pas encore prête, j’ai dû attendre, pendant ce temps, j’ai fait d’autres modèles 3D des prototypes 10 et 11.

Une fois la machine Laser réglée et prête, on nous a appelé que on pouvait utiliser la deuxième machine mais avant ça, on va nous faire une petite formation pour utilisation de cette machine.

## Petite formation deuxième machine Laser ##

Pour ouvrir l’interface de la machine, il faut cliquer, dans l’ordinateur, sur l’application de cette dernière, puis une fois ouvert, il faut juste cliquer sur « nouveau » ou « importer » ou « ouvrir ».

En cliquant sur « nouveau », ça permettrait d’indiquer la taille réelle de mon fichier SVG, c’est ce que je fais.
En cliquant sur « ouvrir » ou « importer » ouvre bien le fichier mais on n’a pas de référence pour l’échelle.

Une fois l’échelle réglée, on clique sur « imprimer » et sélectionner le nom de la machine Laser pour ouvrir les paramètres de découpe. L’avantage de cette machine, c’est qu’elle a une caméra fixée sur le plan de travail de découpe lorsqu’on ferme le couvercle, ça nous permet de bien orienter et placer le fichier SVG sur le matériau qu’on souhaite découper.

Une fois le fichier SVG bien placé, il est important de cliquer sur l’option « pluging » et « vector ».
Ensuite régler la puissance, la vitesse et la fréquence souhaitées, puis ne pas oublier d’allumer le moteur de la machine, ça a peut-être lien avec le Laser.

Et ne pas oublier de nommer le fichier pour le repérer dans la liste de découpes dans l’interface de la machine puis cliquer sur « Print » pour exporter le fichier de découpes en question vers la liste de découpes dans l’interface de la machine.

On sélectionne le fichier en tapant sur l’écran de la machine Laser puis appuyer sur le bouton Start/Stop pour démarrer la découpe. Voilà comment s’est conclue la formation. J’avais plus ou moins bien compris comment fonctionne la machine et j’étais prêt à essayer.

Je mets d’abord un de mes panneaux de carton gris 3mm dans le couvercle et j’insère la clé USB au monteur pour l’ouvrir dans l’interface de la machine, croyant que l’échelle est automatiquement réglée. J’ai directement cliqué sur « imprimer » et sélectionner le nom de la machine.

Je place mon fichier SVG en direct sur le panneau à l’aide de la caméra, j’ai constaté que l’échelle n’était pas juste, j’ai alors annulé pour vérifier. J’ai pensé qu’il était préférable de créer un nouveau fichier pour y paramétrer la taille réelle du panneau 650x500 mm avant de y importer le fichier SVG.

Là, on voit clairement que l’échelle n’était pas juste, j’ai mesuré à l’aide d’un outil de mesure, il est indiqué que la largeur fait 75 mm alors que ça devrait mesurer 100 mm de large, il fallait l’agrandir de 25%.

Malgré les explications d’Axel et de ses calculs très compliqués, j’en ai perdu le fil. Il m’a indiqué qu’il fallait appliquer un agrandissement de 133,33 %. Je n’ai pas compris comment il a fait pour en arriver à cette conclusion. Je pensais y mettre 125% vu qu’il manquait 25 %.

En appliquant cet agrandissement de 133,33 %, l’échelle est cette fois réelle et juste, Axel avait raison, je l’ai remercié.

J’enregistre et clique sur « imprimer pour retourner à la machine, je place le fichier SVG sur le panneau et je nomme mon fichier. Ensuite, je règle la vitesse à 60 %, la puissance à 70 % et la fréquence à 70 % aussi, c’est ma première fois, il fallait tester.

Je clique sur « print » et allume la machine, je me dirige ensuite sur l’écran de la machine pour sélectionner le nom du fichier que j’avais tapé avant d’appuyer sur Start/Stop pour commencer la découpe.

J’étais stupéfait par la vitesse de découpe de cette machine-là, rien à voir avec l’autre machine en panne. Ça avait l’air puissant et c’était net et précis ! ça a à peine duré une minute, dix fois plus rapide que l’autre alors que j’avais mis que 60 % de la vitesse, impressionnant !
Mais là encore, il y a un problème, contre toute attente, le Laser n’a pas pu percer totalement, il manquait soit de puissance, soit c’était trop rapide pour que le Laser ait le temps de traverser le matériau.

J’ai dû refaire le tour deux fois de plus pour en venir à bout. Malgré cela, je suis satisfait de la propreté du découpage. C’est grâce au réglage automatique de la machine qui règle la hauteur idéale pour rendre un travail net et précis.

Il me restait que deux autres découpes à effectuer, mais je dois changer les réglages. Je remets un autre panneau de la même matière et même épaisseur.
J’applique le même agrandissement d’échelle que le précédent fichier. Cette fois je règle la vitesse à 30 %, la puissance et la fréquence à 100 %. On va voir ce que ça va donner.

Cette fois, il n’a fallu à la machine de faire que 2 tours pour venir à bout du carton gris de 3mm.
Le résultat est toujours aussi propre !

J’ai fait pareil pour la 3e découpe, deux fois le tour, ça ne prenait qu’une minute 30 secondes le tour, soit 30 secondes de plus. Je pense qu’à l’avenir, je mettrais la vitesse à 15 % voire 10 % pour que la découpe se fasse bien en un tour.

Je récupère les dernières pièces pour pouvoir commencer à assembler les maquettes, je commence par celles que je préfère, le meilleur siège droit et le meilleur siège long. Je devais faire vite car c’est bientôt mon tour de correction. La panne m’a beaucoup compliqué la vie. J’ai collé l’assemblage du siège droit alors que c’était déconseillé, je l’ignorais. Heureusement, je n’ai pas collé totalement la maquette du siège long.

Voilà, l’heure de mon tour de correction est arrivée, je me dirige vers l’ateliers des professeurs avec mes maquettes, y compris les pièces détachées. Je commence à présenter mes deux maquettes, mon meilleur siège droit et mon meilleur siège long. Les voici ci-dessous.

![git](../docs/images/MAQUETTE_7.jpg)
![git](../docs/images/MAQUETTE_8.jpg)
![git](../docs/images/MAQUETTE_9.jpg)
![git](../docs/images/MAQUETTE_10.jpg)
![git](../docs/images/MAQUETTE_11.jpg)
![git](../docs/images/MAQUETTE_12.jpg)

J’ai expliqué le procédé d’assemblage et le nombre de pièces, ainsi que le nombre de panneaux multiplex nécessaires, etc.

Comme prévu, Victor Levy a préféré le siège long, les assistantes m’avaient dit, si je me souviens bien, que c’était top et bien évolué.
Ce siège long nécessiterait deux grands panneaux multiplex de 244x122 cm, je leur ai expliqué qu’il va me falloir débourser dans les 100 euros pour les deux panneaux.

On m’a dit que le fablab me rembourserait le reste des panneaux non utilisés, environ 20-25 % donc dans les 25 euros. Je n’ai pas d’autres choix, il me reste qu’à aller chez Brico acheter ces panneaux de 18 mm.

Mais avant cela, je dois tout remettre en ordre les dessins des pièces de mon siège définitif. Je les mets à l’échelle réelle avant de les enregistrer au format SVG, je pensais enregistrer pièce par pièce pour mieux les séparer les uns des autres. Je pensais qu’il fallait procéder de cette façon, vu que c’est la première fois que j’utilise une machine Sharper Origin. J’avais hâte de l’essayer.

Par précaution, j’ai pensé d’abord essayer avec des chutes de bois pour mieux comprendre le fonctionnement de la machine avant d’utiliser sur mes panneaux, ceci afin d’éviter le gâchis en cas d’erreur.

J’ai oublié de préciser que lors de la correction, j’avais demandé s’il fallait coller l’assemblage avec de la colle à bois, on m’a dit que non, qu’il fallait laisser comme ça, sans colle. Je me suis dit que ça allait être juste, car j’ai quand même un doute quant à l’assemblage sans colle, car sur la maquette si on met du poids sur le milieu ou un peu plus haut du dossier, ça lâche. On m’a rassuré que l’assemblage réelle tiendra mieux qu’en maquette et que c’est normal.

Au Brico, il n’y avait plus de panneau multiplex 244x122x1.8 cm, ça m’avait angoissé un peu mais j’ai fini par trouver une autre solution, je m’étais arrangé avec mon père pour acheter les panneaux dans un autre magasin de bricolage et me déposer au fablab avec les panneaux découpés le lendemain matin.

En attendant, j’ai l’occasion d’essayer la machine Shaper Origin. Je fais un essai sur un panneau multiplex de 5,5 mm, je penche pour une mèche de 3 mm pour plus de précision sur ce type de panneau.

J’avais réduit les pièces de la chaise à l’échelle correspondant à l’épaisseur de 5,5 mm pour tester.

Je prépare le plan de travail, je pose le panneau sur la table, l’immobilise à l’aide des vis, je me suis dit que ça va être compliqué avec une épaisseur de 18 mm et le dépassement de la vis pourrait nuire au mouvement de la machine Shaper Origin s’ils se touchent.

Au départ, je croyais qu’il fallait mettre des tapes, non sur le plancher ou je vais fraiser mais devant le panneau. Erreur de ma part, c’est pour ça que mon test n’a pas bien fonctionné. Le scanner ne parvenait pas à reconnaitre le plan de travail.

Grace à ça, j’ai mieux compris le fonctionnement de la machine. On m’a mis en garde de ne pas dépasser la profondeur de fraisage le diamètre de la mèche, ça pourrait briser cette dernière.

## VENDREDI 15 JANVIER 2021 ##

On m’avait proposé lors de la correction le jour précédent, de réserver le Shaper Origin de 14h à 18h. même je doutais que ce serait suffisant, c’est l’occasion en or, car on n’ouvre pas le Fablab les vendredis après-midi.

En matinée, je m’occupe de trouver des panneaux avec mon père, on en a trouvé chez Gamma, ils sont de belle qualité comparée de chez Brico. Le prix par panneau 244x122x1,8 cm : 56 euros.

C’est cher pour moi mais raisonnable pour cette qualité. Nous sommes arrivés à 11h40 devant l’entrée de l’ancienne caserne, on a vite déchargé les panneaux découpés de la voiture. 

J’ai oublié de préciser que la veille, j’ai eu des problèmes avec mon fichier SVG avec la Shaper Origin. En effet, les tracés de la pièce ne sont pas liés donc quand je fraise, je dois à chaque fois rétracter la fraise après chaque tracé pour repasser au tracé suivant. C’est gonflant, mais ce n’est pas le pire, je ne pouvais non plus fraiser l’extérieur ou l’intérieur des lignes, c’est fraisé seulement sur la ligne donc dans ce cas, je bâclerais mon travail et l’assemblage serait impossible. C’est très problématique. J’avais besoin d’aide.

Heureusement que j’étais arrivé plus tôt que prévu, j’avais encore deux heures avant la réservation de la machine de 14h à 18h. J’ai profité de ce lap de temps pour demander l’aide aux autres élèves pour m’expliquer comment rendre mes tracés liés dans le SVG de la machine Shaper.

Une étudiante s’est portée volontaire, je l’avais vu travailler avec le Shaper, ses tracés étaient liés, donc elle savait comment faire. Elle m’a dit qu’elle avait travaillé sur fusion 360. Moi, sur Autocad et Sketchup.

Heureusement, elle a dit qu’il était possible d’importer des fichiers Sketchup dans fusion 360, ça m’a évité des heures sup pour recréer les pièces dans Fusion 360. Mais l’importation n’a pas bien fonctionné, on n’arrivait pas à sélectionner toute la face du modèle 3D pour pouvoir l’enregistrer au format SVG avec le bouton Shaper Origin dans un des paramètres de Fusion 360. 

Je n’avais pas de bouton spécifique, je devais le télécharger pour l’installer dans le programme Fusion 360. Une autre solution s’impose, après avoir téléchargé Shaper Origin pour Fusion 360, je peux importer les fichiers, les pièces au format DWG. 

C’est un succès ! Les plans sont bien importés dans le programme, je vérifie l’échelle, c’est juste, il me reste qu’à extruder les faces de 18mm, l’épaisseur réelle du panneau multiplex. Ensuite avec le bouton Shaper Origin (voir ci-dessous)

![git](../docs/images/CAPTURE_FUSION_360_SHAPER_ORIGIN.JPG)

Je sélectionne une face d’une des pièces, puis clique sur OK et c’est automatiquement enregistré au format SVG spécifié pour la machine Shaper Origin. J’ai terminé et je remercie gracieusement cette étudiante pour m’avoir aidé.

## MON PREMIER FRAISAGE POUR DE VRAI. ##

Je prépare le plan de travail sur la table, je commence par le dossier. Voir plan de la pièce ci-dessous.

![git](../docs/images/CAPTURE_PIECE_2.JPG)

Je prends le panneau 61x99 cm, le place sur la table à l’aide des doubles tapes pour l’immobiliser lors du fraisage. Je place aussi un panneau 23,5x1450 cm de la même épaisseur comme support de base de la machine, ça permettra à la machine de scanner proprement le plan de travail devant elle. Après avoir collé les shapertapes sur le panneau 61x99 cm, je peux commencer à scanner. 

J’utilise la mèche de 6mm. Je sélectionne le fichier SVG du dossier de la chaise pour l’importer sur le plan de travail, je règle la profondeur de fraisage à 6mm. Je vais devoir faire trois fois le tour pour fraiser totalement. Après avoir appuyé sur Touch Z, je peux commencer à fraiser.

C’était assez dur de déplacer la machine sans y mettre un peu de force, mais assez facile de suivre le tracé depuis l’écran. Le plus embêtant, c’est quand la machine réclame le manque de Shaper tape, ça interrompt à chaque fois mon travail pour que j’en rajoute de nouveaux.

J’ai dû rajouter un panneau devant le panneau fraisé pour y mettre des Shapertapes supplémentaires, mais j’ai commis une grosse erreur ! Je n’ai pas fixé le panneau devant, pensant que ça tiendrait sans double tap, je n’avais pas immobilisé le panneau.

J’ai fait une autre erreur de débutant, j’ai retiré du Shapertape qui dérangeait et qui s décollait. Ça a buggé le scan de la machine.

J’ai dû recommencer le scan et replacer le dessin dessus en faisant de mon mieux pour le placer à un peu près au même endroit. Il y aura des marges d’erreurs mais je n’ai pas le choix.

Hélas, ce dossier est raté car le panneau devant avait bougé, ça avait crée un gros décalage des deux cotés du dossier. Ce fut un gros gâchis, un échec, mais ça m’a permis d’apprendre trois choses :

-	Toujours bien immobiliser le plan de travail !
-	Ne jamais enlever le shapertape qui gêne !
-	Mettre autant de shapertape sur le plan de travail pour améliorer la qualité de scan et de fraisage si nécessaire.
-	
Comme la table était assez instable, j’ai décidé de travailler par terre, là ou c’est bien plat et stable.

La préparation du plan de travail sur le sol m’a pris beaucoup de temps, je l’ai bien immobilisé avec des doubles taps et j’ai bien sur utilisé des chutes de bois de 5,5 mm sous les panneaux de 18 mm pour protéger le sol.

Ces panneaux sont collés entre eux avec des doubles taps, j’ai utilisé deux panneaux 145x23.5x1.8, l’un comme base de la machine devant moi et l’autre juste devant le panneau de 99x61 cm à fraiser. J’ai laissé les shapertapes sur le panneau.

Je positionne le dessin du repose-pied au format SVG sur le plan de travail, sur ce panneau 99x61 cm.

Voici le plan du repose-pied.

![git](../docs/images/CAPTURE_PIECE_3.JPG)

Cette fois, j’ai réussi à fraiser toute la pièce, j’ai dû faire 3 fois le tour pour percer les 18 mm en commençant par 6 mm de profondeur, puis 12 mm et ensuite 18,5 mm.

Ça m’a pris beaucoup de temps, de force et de patience aussi pour faire ce très lent tour. Cette pièce m’a posé moins de problème grâce aux erreurs que j’avais commises avec le dossier. Ça m’a donné de l’expérience.

Je nettoie le plan de travail, remplace le panneau fraisé par un autre afin de fraiser le support des pieds arrières de la chaise. Voici la pièce en question ci-dessous.

![git](../docs/images/CAPTURE_PIECE_1.JPG)

Le fraisage de cette pièce ne m’a non plus posé de problème mais ça m’avait épuisé de fraiser au sol, ça m’a pompé beaucoup d’énergie et j’ai les genoux en compote, je penserai à amener un coussin pour un meilleur confort pour mes genoux.

Il était déjà presque 18h, fermeture du fablab. Je n’avais pas fraisé tout, c’était impossible de tout faire en 4h, mes erreurs m’on retardés. 

J’ai demandé à Victor Levy comment et quand je pourrai fraiser mes pièces restantes vu que tout le monde a déjà réservé la Shaper toute la semaine suivante, il a répondu que peut être la seule solution serait de le faire mercredi 20 janvier toute la journée, le jour de la remise au musée.

Il m’a suggéré d’envoyer un mail à Gwen et Hélène pour s’organiser avec elles, je l’ai fait et il me restait qu’à attendre leur réponse.

Ainsi se conclut la journée.

## LUNDI 18 JANVIER 2021 ##

Je n’ai pas reçu de réponse de la part de Gwen et Hélène, après mon jury d’architecture d’intérieur à l’académie royale des beaux-arts de Bruxelles, je suis allé au Fablab voir ce que je peux faire.

La machine shaper origin est occupée, j’ai demandé à Gwen s’elle avait reçu mon mail, elle venait justement le lire, je lui ai fait remarquer qu’il y avait une deuxième machine shaper Origin, elle l’ignorait. Alors je lui ai indiqué ou elle est et Gwen va voir avec Axel pour régler la machine voir s’on peut l’utiliser. Si ce serait le cas, je pourrais l’utiliser toute la journée le lendemain.

En attendant, j’ai profité de poncer manuellement tous les bords coupants des deux pièces que j’avais déjà fraisé vendredi. Ça rend le visuel plus élégant et plus propre ! 

Gwen m’a donné le feu vert pour l’utilisation de la deuxième machine, elle a dit que je pouvais déjà commencer aujourd’hui. Il était 15h30, j’avais le temps pour fraiser une ou deux pièces, on m’a donné un grand atelier où je pouvais travailler à l’aise car la salle CNC était à l’étroit et rempli.
Je prépare le même plan de travail que vendredi et commence à fraiser la pièce, l’assise de la chaise. Voir plan ci-dessous

![git](../docs/images/CAPTURE_PIECE_4.JPG)

Le fraisage de cette pièce s’est déroulé à merveille, j’ai bien compris le fonctionnent de la machine, j’ai bien progressé et j’apprécie beaucoup !

Ça m’a quand même pris un bon bout de temps, il était déjà 17h passé, plus que 1h avant la fermeture du Fablab. J’avais jugé que je n’aurais pas le temps de finir de fraiser le dossier de la chaise. Donc j’ai stoppé, je préparais juste le plan de travail pour reprendre directement à mon arrivée au Fablab le lendemain matin à 9h30.

Il me restait juste assez de temps pour poncer les bords de la pièce de l’assise fraichement fraisée.
Après ça, ma journée était terminée.

## MARDI 19 JANVIER 2021 ##

Le matin, juste avant d’arriver au Fablab, je suis allé au Brico acheter du double tab, j’en avais besoin, il n’y en avait plus. A mon arrivée au Fablab, comme mon plan de travail était déjà prête, j’ai directement, sans attente, commencé à fraiser le dossier de la chaise.

Ça s’est passé sans encombre, le résultat était top sauf que c’était le même panneau où j’avais fraisé et raté le dossier la semaine dernière. Ça y avait laissé des traces de fraisage mais je n’avais pas le choix, c’était une question d’économie.

Le plus gros travail est à venir, le fraisage des deux grands pieds de la chaise, j’ai préparé le plan de travail et utilisé le tout nouveau double tap, ça colle super bien !

J’importe le dessin du grand pied sur le plan de travail. Voici le plan du pied ci-dessous.

![git](../docs/images/CAPTURE_PIECE_5.JPG)

Je commence par fraiser de l’intérieur les trous triangulaires et rectangulaires avant de fraiser de l’extérieur le contour de la pièce, j’ai fait une gaffe, j’avais fraisé une petite partie du mauvais côté, j’avais oublié de modifier l’option « de l’intérieur » à « de l’extérieur », heureusement cette erreur ne se verrait pas trop.

A part de la gaffe, ça s’est plutôt bien passé, après avoir tout nettoyé, je repasse le même procédé de travail pour le deuxième grand pied.

Le fraisage de ce deuxième grand pied s’est passé à merveille mais j’ai encore fait une gaffe, je voulais soulever le panneau et retirer la pièce fraisée du plan de travail mais la colle de la double tap était trop forte que j’ai dû forcer un peu le retrait de la pièce. Ça a arraché une partie d’une couche de multiplex, ça m’a dégouté.

Mais c’était un peu près réparable avec de la colle à bois et deux presses. En attendant que la colle séchée, j’ai poncé manuellement tous les bords coupants du dossier et du grand pied de la chaise.

Ça m’a pris une bonne heure, je retire les presses, la colle a bien séché et ça paraît solide. Je ponce cette dernière pièce et il me restait qu’à espérer que l’assemblage fonctionnera sans encombre.

L’assemblage était très dur que je m’y attendais, j’ai dû recourir au marteau pour les faire entrer entre eux.  J’ai un peu sué mais ça a bien marché sauf que le bord haut du repose-pieds n’était pas droit. Ce n’était pas grave, j’ai utilisé une ponceuse mécanique pour régler le problème.

Une fois réglée, le résultat est magnifique et propre, je trouve.
Je suis super content du résultat ! j’étais fier d’en arriver là.

Voici le résultat final

![git](../docs/images/RESULTAT_FINAL_3.jpg)

Il me reste à remettre le siège et tous les prototypes au musée le lendemain. Et on verra au jury jeudi 21 janvier.
Merci pour la lecture, j’ai pris du plaisir d’écrire d’abord manuellement puis numériquement tous ces textes. Mon stylo-plume en a bavé.







