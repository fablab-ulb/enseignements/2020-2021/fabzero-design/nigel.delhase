# PRESENTATION PERSONNELLE #

Bonjour à tous, 

je me nomme Nigel Delhasse, je suis étudiant en Master 2 d’architecture d’intérieur à l’académie royale des beaux-arts de Bruxelles.

Je suis actuellement en séminaire à ULB tous les jeudis, c’est la première fois que j’y suis les cours, c’est assez ardu comparé à l’académie Royale des Beaux-Arts.

J’ai choisi ce Séminaire Architecture et Design en charge par Victor Levy car j’ai toujours voulu essayer ce procédé, j’ai envie d’expérimenter.

Je suis un cas particulier à cause de ma surdité et il se peut que ce soit la première fois pour vous d’accueillir un élève sourd. Je suis toujours accompagné d’une interprète en langue de signes, ce qui peut parfois surprendre. Cette dernière se présente toujours au début des cours.

## MON ŒUVRE CHOISIE ##

![git](../docs/images/PHOTO_CHAISE_1.jpg)
 
Une chaise à bascule, appelé Endless Flow de l’artiste Designer Dirk Vander Kooij, cette chaise a été créé en 2011 au centre d’innovation et de Design au Grand Hornu à Hainaut en Belgique. Basée de matériaux en plastique recyclé et réutilisé à l’imprimante 3D

Le designer, Dirk Vander Kooij, d’origine Hollandaise a été diplômé à l’académie du design d’Eindhoven. Il travaille surtout au design mobilier basé de matière en plastique recyclé. Il a conquis et programmé une vieille machine robotique venant d’une usine chinoise pour en faire un appareil d’impression 3D géant automatisé. L’artiste n’avait qu’à programmer et régler la machine pour créer une forme qu’il souhaite et la machine fait le travail en fondant et éjectant la matière plastique fondu de couche en couche.

En 2011, il a conçu la chaise Endless Flow, il y en a plusieurs de formes différentes, cette œuvre a été accablée gagnante au Dutch Design Award.
 
![git](../docs/images/PHOTO_CHAISE_2.jpg)

Voici une autre chaise d’une forme différente, appelée « the chubby chair », conçue à un peu près la même méthode mais plus rapidement et simplement. Les couches sont plus épaisses et sont soutenues par les supports pour les tenir sans qu’elles s’écoulent avant de se solidifier pour l’assemblage.

Pourquoi j’ai choisi cette œuvre, parce que j’aime beaucoup l’idée de réutiliser les déchets en plastique en les recyclant pour leur donner une nouvelle vie et une nouvelle utilité au quotidien avec la technologie d’impression 3D au lieu de les jeter inutilement.
Cette méthode pourrait répondre à la problématique actuelle de la crise des déchets en plastique dans le monde. L’impression 3D m’a toujours fasciné depuis son invention, avec cette technologie, on peut créer plein de formes qui étaient presque impossible à concevoir, la possibilité est plus large. J’aimerais me former pour utiliser cette technologie.

Au début, j’avoue que je n’aimais pas trop cette chaise à bascule, c’était juste le procédé de création qui m’intéressait mais après avoir fait des recherches sur Dirk Vander Kooij et ses œuvres, ses mobiliers en plastique m’ont complètement conquis, je les adore ! Surtout la table ci-dessous.

![git](../docs/images/PHOTO_TABLE.jpg)
 
D’habitude, je déteste les mobiliers en plastique, c’est vulgaire, ça me dégoutait mais là Dirk m’a prouvé qu’on peut les rendre beaux.
La table a été conçu avec des éléments de la Chubby Chair, des vinyles, d’autres objets en plastique et on les a tout fondu et versé dans un support creux d’une forme spécifique avant de l’enlever une fois la matière solidifiée pour l’étape de l’assemblage. J’adore le résultat visuel, on dirait du marbre mais non c’est du plastique !

Regarder [vidéo](https://www.youtube.com/watch?v=92SLUXnzfmY&ab_channel=FlindersDesign) pour voir le procédé d'impression de l'Endless Chair.
